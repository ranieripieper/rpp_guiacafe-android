package com.doisdoissete.android.util.ddsutil;

import java.text.Normalizer;

public class StringUtil {

	public static String getNormalizeStr(String value) {
		if (value != null) {
			value = Normalizer.normalize(value, Normalizer.Form.NFD);
			value = value.replaceAll("[^\\p{ASCII}]", "");
			value = value.toLowerCase().replaceAll(" ", "_");
		}
		
		return value;
	}
}
