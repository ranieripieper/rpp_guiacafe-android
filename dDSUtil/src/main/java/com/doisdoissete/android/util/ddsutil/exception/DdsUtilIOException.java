package com.doisdoissete.android.util.ddsutil.exception;

import org.springframework.http.HttpStatus;

public class DdsUtilIOException extends java.io.IOException {

	private static final long serialVersionUID = 1L;
	private String msgErro;
	private HttpStatus httpStatus;
	
	public DdsUtilIOException() {
		
	}
	
	public DdsUtilIOException(String m) {
		super();
		this.msgErro = m;
	}
	
	public DdsUtilIOException(Exception e) {
		super(e);
	}
	
	public DdsUtilIOException(Exception e, String msg, HttpStatus httpStatus) {
		super(e);
		this.msgErro = msg;
		this.httpStatus = httpStatus;
	}
	
	public DdsUtilIOException(String msg, HttpStatus httpStatus) {
		this.msgErro = msg;
		this.httpStatus = httpStatus;
	}

	public String getMsgErro() {
		return msgErro;
	}

	public void setMsgErro(String msgErro) {
		this.msgErro = msgErro;
	}

	public HttpStatus getHttpStatus() {
		return httpStatus;
	}

	public void setHttpStatus(HttpStatus httpStatus) {
		this.httpStatus = httpStatus;
	}

	
}