package com.doisdoissete.android.util.ddsutil.view.custom;

public interface FixedHeaderScrollObserver {

	void onResizeHeader(int scroll, int actual, int newValue);
	void onPreMoveHeader();
	void onPostMoveHeader(int oldTop, int transY);
}
