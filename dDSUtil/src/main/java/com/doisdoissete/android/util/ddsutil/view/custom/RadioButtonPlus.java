package com.doisdoissete.android.util.ddsutil.view.custom;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.RadioButton;

import com.doisdoissete.android.util.ddsutil.R;
import com.doisdoissete.android.util.ddsutil.view.custom.util.FontUtilCache;

public class RadioButtonPlus extends RadioButton {
	
	private static final String TAG = "RadioGroupPlus";

	public RadioButtonPlus(Context context) {
		super(context);
	}

	public RadioButtonPlus(Context context, AttributeSet attrs) {
		super(context, attrs);
		setCustomFont(context, attrs);
	}

	public RadioButtonPlus(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		setCustomFont(context, attrs);
	}

    private void setCustomFont(Context ctx, AttributeSet attrs) {
        TypedArray a = ctx.obtainStyledAttributes(attrs, R.styleable.TextViewPlus);
        TypedArray defaultAttr = ctx.obtainStyledAttributes(attrs, R.styleable.DDSDefaultAttributes);
        String customFont = defaultAttr.getString(R.styleable.DDSDefaultAttributes_text_font);
        defaultAttr.recycle();
        FontUtilCache.setCustomFont(ctx, this, customFont);
        a.recycle();
    }

}
