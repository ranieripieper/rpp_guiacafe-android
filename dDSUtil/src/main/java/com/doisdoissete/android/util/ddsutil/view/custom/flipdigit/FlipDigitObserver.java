package com.doisdoissete.android.util.ddsutil.view.custom.flipdigit;

public interface FlipDigitObserver {

	void onNexScroll(int nr);
	
}
