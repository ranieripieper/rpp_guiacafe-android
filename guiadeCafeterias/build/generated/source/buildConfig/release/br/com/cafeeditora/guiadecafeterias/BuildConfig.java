/**
 * Automatically generated file. DO NOT MODIFY
 */
package br.com.cafeeditora.guiadecafeterias;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "br.com.cafeeditora.guiadecafeterias";
  public static final String BUILD_TYPE = "release";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 3;
  public static final String VERSION_NAME = "1.2";
}
