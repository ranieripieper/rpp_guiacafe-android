package br.com.cafeeditora.guiadecafeterias;

import java.util.Date;

import android.app.Application;
import br.com.cafeeditora.guiadecafeterias.util.Constants;
import br.com.cafeeditora.guiadecafeterias.util.Preferences;
import br.com.cafeeditora.guiadecafeterias.view.activity.DetailStoreActivity;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

public class GuiaCafeteriasApplication extends Application {  

	public static ImageLoader imageLoader;
	
    @Override
    public void onCreate() {
        super.onCreate();
        
        DisplayImageOptions options = new DisplayImageOptions.Builder()
        	.showImageOnLoading(R.drawable.img_place_holder) // resource or drawable
        	.showImageForEmptyUri(R.drawable.img_place_holder) // resource or drawable
        	.showImageOnFail(R.drawable.img_place_holder) // resource or drawable
	        .resetViewBeforeLoading(false)  // default
	        .delayBeforeLoading(100)
	        .considerExifParams(true)
	        
	        .cacheInMemory(true) // default
	        .cacheOnDisc(true).build(); // default

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
                .defaultDisplayImageOptions(options) // default
                .writeDebugLogs()
                
                .build();
        
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(config);
        
		Date dt = Preferences.getLastClearCache(this);
		
		if (dt == null || (int)((new Date().getTime() - dt.getTime())/ (24*60*60*1000)) > Constants.DIAS_CACHE) {
        	GuiaCafeteriasApplication.imageLoader.clearDiscCache();
        	GuiaCafeteriasApplication.imageLoader.clearMemoryCache();
            Preferences.setLastClearCache(this, new Date());
        }
		
		Preferences.setFavDesfavClicked(this, false);
    }
}   
