package br.com.cafeeditora.guiadecafeterias;

import br.com.cafeeditora.guiadecafeterias.view.activity.MainActivity;

public class SplashScreenActivity extends com.doisdoissete.android.util.ddsutil.view.SplashScreenActivity {

    @Override
    protected int getContentView() {
        return R.layout.splash_screen_activity;
    }

    protected void startMainActivity() {
        MainActivity.showActivity(this);
        overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
    }

	/*
    @Override
	protected int getSplashScreenTimeOut() {
		if (BuildConfig.DEBUG) {
			return 100;
		}
		return  super.getSplashScreenTimeOut();
	}*/
}
