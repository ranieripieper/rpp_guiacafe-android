package br.com.cafeeditora.guiadecafeterias.model;

import android.os.Parcel;
import android.os.Parcelable;
import br.com.cafeeditora.guiadecafeterias.R;

import com.doisdoissete.android.util.ddsutil.StringUtil;

public class Address implements Parcelable {
	
	private String street;
	private String number;
	private String complement;
	private String district;
	private String city;
	private String state;
	private String zip;
	private Geolocation geolocation;
	/**
	 * @return the street
	 */
	public String getStreet() {
		return street;
	}
	/**
	 * @param street the street to set
	 */
	public void setStreet(String street) {
		this.street = street;
	}
	/**
	 * @return the number
	 */
	public String getNumber() {
		return number;
	}
	/**
	 * @param number the number to set
	 */
	public void setNumber(String number) {
		this.number = number;
	}
	/**
	 * @return the complement
	 */
	public String getComplement() {
		return complement;
	}
	/**
	 * @param complement the complement to set
	 */
	public void setComplement(String complement) {
		this.complement = complement;
	}
	/**
	 * @return the district
	 */
	public String getDistrict() {
		return district;
	}
	/**
	 * @param district the district to set
	 */
	public void setDistrict(String district) {
		this.district = district;
	}
	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}
	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}
	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}
	
	/**
	 * @return the state
	 */
	public int getStateToCompare() {
		state = StringUtil.getNormalizeStr(state);
		if ("acre".equals(state) || "ac".equals(state)) {
			return R.string.acre;
		} else if ("alagoas".equals(state) || "al".equals(state)) {
			return R.string.alagoas;
		} else if ("amapa".equals(state) || "ap".equals(state)) {
			return R.string.amapa;
		} else if ("amazonas".equals(state) || "am".equals(state)) {
			return R.string.amazonas;
		} else if ("bahia".equals(state) || "ba".equals(state)) {
			return R.string.bahia;
		} else if ("ceara".equals(state) || "ce".equals(state)) {
			return R.string.ceara;
		} else if ("df".equals(state) || "distrito_federal".equals(state)) {
			return R.string.distrito_federal;
		} else if ("espirito_santo".equals(state) || "es".equals(state)) {
			return R.string.espirito_santo;
		} else if ("goias".equals(state) || "go".equals(state)) {
			return R.string.goias;
		} else if ("maranhao".equals(state) || "ma".equals(state)) {
			return R.string.maranhao;
		} else if ("mato_grosso".equals(state) || "mt".equals(state)) {
			return R.string.mato_grosso;
		} else if ("mato_grosso_do_sul".equals(state) || "ms".equals(state)) {
			return R.string.mato_grosso_do_sul;
		} else if ("minas_gerais".equals(state) || "mg".equals(state)) {
			return R.string.minas_gerais;
		} else if ("para".equals(state) || "pa".equals(state)) {
			return R.string.para;
		} else if ("paraiba".equals(state) || "pb".equals(state)) {
			return R.string.paraiba;
		} else if ("parana".equals(state) || "pr".equals(state)) {
			return R.string.parana;
		} else if ("pernambuco".equals(state) || "pe".equals(state)) {
			return R.string.pernambuco;
		} else if ("piaui".equals(state) || "pi".equals(state)) {
			return R.string.piaui;
		} else if ("rio_grande_do_norte".equals(state) || "rn".equals(state)) {
			return R.string.rio_grande_do_norte;
		} else if ("rio_grande_do_sul".equals(state) || "rs".equals(state)) {
			return R.string.rio_grande_do_sul;
		} else if ("rj".equals(state) || "rio_de_janeiro".equals(state)) {
			return R.string.rio_de_janeiro;
		} else if ("rondonia".equals(state) || "ro".equals(state)) {
			return R.string.rondonia;
		} else if ("roraima".equals(state) || "rr".equals(state)) {
			return R.string.roraima;
		} else if ("santa_catarina".equals(state) || "sc".equals(state)) {
			return R.string.santa_catarina;
		} else if ("sergipe".equals(state) || "se".equals(state)) {
			return R.string.sergipe;
		} else if ("sp".equals(state) || "sao_paulo".equals(state)) {
			return R.string.sao_paulo;
		} else if ("tocantins".equals(state) || "to".equals(state)) {
			return R.string.tocantins;
		}
		return R.string.sao_paulo;
	}
	/**
	 * @param state the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}
	/**
	 * @return the zip
	 */
	public String getZip() {
		return zip;
	}
	/**
	 * @param zip the zip to set
	 */
	public void setZip(String zip) {
		this.zip = zip;
	}
	/**
	 * @return the geolocation
	 */
	public Geolocation getGeolocation() {
		return geolocation;
	}
	/**
	 * @param geolocation the geolocation to set
	 */
	public void setGeolocation(Geolocation geolocation) {
		this.geolocation = geolocation;
	}

	/******************************************/
    /********** Parcelable interface **********/
    /******************************************/

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeString(this.city);
        out.writeString(this.complement);
        out.writeString(this.district);
        out.writeString(this.number);
        out.writeString(this.state);
        out.writeString(this.street);
        out.writeString(this.zip);
        out.writeParcelable(this.getGeolocation(), flags);
    }

    private static Address readFromParcel(Parcel in) {
    	Address obj = new Address();
    	obj.setCity(in.readString());
    	obj.setComplement(in.readString());
    	obj.setDistrict(in.readString());
    	obj.setNumber(in.readString());
    	obj.setState(in.readString());
    	obj.setStreet(in.readString());
    	obj.setZip(in.readString());
    	obj.setGeolocation((Geolocation)in.readParcelable(Geolocation.class.getClassLoader()));
        return obj;
    }

	public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
		public Address createFromParcel(Parcel in) {
			return readFromParcel(in);
		}

		public Address[] newArray(int size) {
			return new Address[size];
		}
	};
	
}
