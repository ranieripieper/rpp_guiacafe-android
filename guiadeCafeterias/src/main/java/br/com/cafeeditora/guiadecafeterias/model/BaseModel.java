package br.com.cafeeditora.guiadecafeterias.model;

import android.text.TextUtils;

public class BaseModel {

	protected String getText(String txt) {
		if (TextUtils.isEmpty(txt)) {
			return "";
		} else {
			return txt;
		}
	}
}
