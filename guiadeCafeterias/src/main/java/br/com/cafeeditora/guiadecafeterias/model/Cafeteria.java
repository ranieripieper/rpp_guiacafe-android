package br.com.cafeeditora.guiadecafeterias.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import br.com.cafeeditora.guiadecafeterias.util.Constants;

import com.google.gson.annotations.SerializedName;

public class Cafeteria implements Parcelable, Comparable<Cafeteria> {

	//private static final long serialVersionUID = 7070070776719849407L;

	public static final String PARAM_ID = "{id}";
	public static final String PARAM_ACCESS_TOKEN = "accessToken";
	public static final String PARAM_RATING = "rating";
	
	private String name;
	private String slug;
	private String uuid;
	@SerializedName("formatedAddress")
	private String formatedAddress;
	@SerializedName("formatedHours")
	private String formatedHours;
	@SerializedName("formatedPaymentsCards")
	private String formatedPaymentsCards;
	@SerializedName("formatedPaymentsBrands")
	private String formatedPaymentsBrands;
	@SerializedName("formatedPaymentsTypeTickets")
	private String formatedPaymentsTypeTickets;
	private String resume;
	
	@SerializedName("isFeatured")
	private boolean isFeatured;
	@SerializedName("isTop20")
	private boolean isTop20;
	@SerializedName("isCafeteriaRevelacao")
	private boolean isCafeteriaRevelacao;
	
	private Contact contact;
	private Address address;	
	private Photo photos[];
	private Informations informations;
	private Establishment establishment;
	private Payment payments;
	private Coffee coffee;
	private Social social;
	
	private float distance;
	
	private Ratings ratings;
	@SerializedName("userData")
	private UserData userData;
	
	public String getLogo() {
		if (photos != null && photos.length > 0 && !TextUtils.isEmpty(photos[0].getUrl())) {
			return Constants.THUMB_HOME_PHOTO.replace("{src}", photos[0].getUrl());
		}
		return "";
	}
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the slug
	 */
	public String getSlug() {
		return slug;
	}
	/**
	 * @param slug the slug to set
	 */
	public void setSlug(String slug) {
		this.slug = slug;
	}
	/**
	 * @return the uuid
	 */
	public String getUuid() {
		return uuid;
	}
	/**
	 * @param uuid the uuid to set
	 */
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	/**
	 * @return the formatedAddress
	 */
	public String getFormatedAddress() {
		return formatedAddress;
	}
	/**
	 * @param formatedAddress the formatedAddress to set
	 */
	public void setFormatedAddress(String formatedAddress) {
		this.formatedAddress = formatedAddress;
	}
	/**
	 * @return the formatedHours
	 */
	public String getFormatedHours() {
		return formatedHours;
	}
	/**
	 * @param formatedHours the formatedHours to set
	 */
	public void setFormatedHours(String formatedHours) {
		this.formatedHours = formatedHours;
	}
	/**
	 * @return the formatedPaymentsCards
	 */
	public String getFormatedPaymentsCards() {
		return formatedPaymentsCards;
	}
	/**
	 * @param formatedPaymentsCards the formatedPaymentsCards to set
	 */
	public void setFormatedPaymentsCards(String formatedPaymentsCards) {
		this.formatedPaymentsCards = formatedPaymentsCards;
	}
	/**
	 * @return the formatedPaymentsBrands
	 */
	public String getFormatedPaymentsBrands() {
		return formatedPaymentsBrands;
	}
	/**
	 * @param formatedPaymentsBrands the formatedPaymentsBrands to set
	 */
	public void setFormatedPaymentsBrands(String formatedPaymentsBrands) {
		this.formatedPaymentsBrands = formatedPaymentsBrands;
	}
	/**
	 * @return the formatedPaymentsTypeTickets
	 */
	public String getFormatedPaymentsTypeTickets() {
		return formatedPaymentsTypeTickets;
	}
	/**
	 * @param formatedPaymentsTypeTickets the formatedPaymentsTypeTickets to set
	 */
	public void setFormatedPaymentsTypeTickets(String formatedPaymentsTypeTickets) {
		this.formatedPaymentsTypeTickets = formatedPaymentsTypeTickets;
	}
	/**
	 * @return the isFeatured
	 */
	public boolean isFeatured() {
		return isFeatured;
	}
	/**
	 * @param isFeatured the isFeatured to set
	 */
	public void setFeatured(boolean isFeatured) {
		this.isFeatured = isFeatured;
	}
	/**
	 * @return the isTop20
	 */
	public boolean isTop20() {
		return isTop20;
	}
	/**
	 * @param isTop20 the isTop20 to set
	 */
	public void setTop20(boolean isTop20) {
		this.isTop20 = isTop20;
	}
	/**
	 * @return the isCafeteriaRevelacao
	 */
	public boolean isCafeteriaRevelacao() {
		return isCafeteriaRevelacao;
	}
	/**
	 * @param isCafeteriaRevelacao the isCafeteriaRevelacao to set
	 */
	public void setCafeteriaRevelacao(boolean isCafeteriaRevelacao) {
		this.isCafeteriaRevelacao = isCafeteriaRevelacao;
	}
	/**
	 * @return the contact
	 */
	public Contact getContact() {
		return contact;
	}
	/**
	 * @param contact the contact to set
	 */
	public void setContact(Contact contact) {
		this.contact = contact;
	}
	/**
	 * @return the address
	 */
	public Address getAddress() {
		return address;
	}
	/**
	 * @param address the address to set
	 */
	public void setAddress(Address address) {
		this.address = address;
	}
	/**
	 * @return the photos
	 */
	public Photo[] getPhotos() {
		return photos;
	}
	/**
	 * @param photos the photos to set
	 */
	public void setPhotos(Photo[] photos) {
		this.photos = photos;
	}
	/**
	 * @return the informations
	 */
	public Informations getInformations() {
		return informations;
	}
	/**
	 * @param informations the informations to set
	 */
	public void setInformations(Informations informations) {
		this.informations = informations;
	}
	
	/**
	 * @return the resume
	 */
	public String getResume() {
		return resume;
	}

	/**
	 * @param resume the resume to set
	 */
	public void setResume(String resume) {
		this.resume = resume;
	}

	/**
	 * @return the establishment
	 */
	public Establishment getEstablishment() {
		return establishment;
	}

	/**
	 * @param establishment the establishment to set
	 */
	public void setEstablishment(Establishment establishment) {
		this.establishment = establishment;
	}

	/**
	 * @return the payments
	 */
	public Payment getPayments() {
		return payments;
	}

	/**
	 * @param payments the payments to set
	 */
	public void setPayments(Payment payments) {
		this.payments = payments;
	}

	/**
	 * @return the coffee
	 */
	public Coffee getCoffee() {
		return coffee;
	}

	/**
	 * @param coffee the coffee to set
	 */
	public void setCoffee(Coffee coffee) {
		this.coffee = coffee;
	}

	/**
	 * @return the social
	 */
	public Social getSocial() {
		return social;
	}

	/**
	 * @param social the social to set
	 */
	public void setSocial(Social social) {
		this.social = social;
	}

	/**
	 * @return the ratings
	 */
	public Ratings getRatings() {
		return ratings;
	}

	/**
	 * @param ratings the ratings to set
	 */
	public void setRatings(Ratings ratings) {
		this.ratings = ratings;
	}

	/**
	 * @return the userData
	 */
	public UserData getUserData() {
		return userData;
	}

	/**
	 * @param userData the userData to set
	 */
	public void setUserData(UserData userData) {
		this.userData = userData;
	}

	/**
	 * @return the distance
	 */
	public float getDistance() {
		return distance;
	}

	/**
	 * @param distance the distance to set
	 */
	public void setDistance(float distance) {
		this.distance = distance;
	}

	/******************************************/
    /********** Parcelable interface **********/
    /******************************************/

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeString(this.name);
        out.writeString(this.formatedAddress);
        out.writeString(this.formatedHours);
        out.writeString(this.formatedPaymentsBrands);
        out.writeString(this.formatedPaymentsCards);
        out.writeString(this.formatedPaymentsTypeTickets);
        out.writeString(this.resume);
        out.writeString(this.slug);
        out.writeString(this.uuid);
        out.writeInt(photos.length);
        out.writeTypedArray(photos, flags);
        out.writeParcelable(this.getAddress(), flags);
        out.writeParcelable(this.getInformations(), flags);
        out.writeParcelable(this.getContact(), flags);
        out.writeParcelable(this.getEstablishment(), flags);
        out.writeParcelable(this.getPayments(), flags);
        out.writeParcelable(this.getCoffee(), flags);
        out.writeParcelable(this.getSocial(), flags);
        out.writeParcelable(this.getRatings(), flags);
        out.writeParcelable(this.getUserData(), flags);
        out.writeByte((byte) (this.isCafeteriaRevelacao ? 1 : 0));
        out.writeByte((byte) (this.isFeatured ? 1 : 0));
        out.writeByte((byte) (this.isTop20 ? 1 : 0));
    }

    private static Cafeteria readFromParcel(Parcel in) {
    	Cafeteria obj = new Cafeteria();
    	obj.setName(in.readString());
    	obj.setFormatedAddress(in.readString());
    	obj.setFormatedHours(in.readString());
    	obj.setFormatedPaymentsBrands(in.readString());
    	obj.setFormatedPaymentsCards(in.readString());
    	obj.setFormatedPaymentsTypeTickets(in.readString());
    	obj.setResume(in.readString());
    	obj.setSlug(in.readString());
    	obj.setUuid(in.readString());
    	obj.setPhotos(new Photo[in.readInt()]);
        in.readTypedArray(obj.getPhotos(), Photo.CREATOR);
        obj.setAddress((Address)in.readParcelable(Address.class.getClassLoader()));
        obj.setInformations((Informations)in.readParcelable(Informations.class.getClassLoader()));
        obj.setContact((Contact)in.readParcelable(Contact.class.getClassLoader()));
        obj.setEstablishment((Establishment)in.readParcelable(Establishment.class.getClassLoader()));
        obj.setPayments((Payment)in.readParcelable(Payment.class.getClassLoader()));
        obj.setCoffee((Coffee)in.readParcelable(Coffee.class.getClassLoader()));
        obj.setSocial((Social)in.readParcelable(Social.class.getClassLoader()));
        obj.setRatings((Ratings)in.readParcelable(Ratings.class.getClassLoader()));
        obj.setUserData((UserData)in.readParcelable(UserData.class.getClassLoader()));
        obj.setCafeteriaRevelacao(in.readByte() != 0);
        obj.setFeatured(in.readByte() != 0);
        obj.setTop20(in.readByte() != 0);
        return obj;
    }

	public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
		public Cafeteria createFromParcel(Parcel in) {
			return readFromParcel(in);
		}

		public Cafeteria[] newArray(int size) {
			return new Cafeteria[size];
		}
	};
	
	//Comparable
	
	@Override
	public int compareTo(Cafeteria another) {
		return Float.compare(this.distance, another.distance);
	}
}
