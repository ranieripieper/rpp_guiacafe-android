package br.com.cafeeditora.guiadecafeterias.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Coffee implements Parcelable {

	@SerializedName("alternateTypes")
	private CoffeeAlternateTypes coffeeAlternateTypes;

	private boolean tnt;
	private boolean tasting;
	private boolean roasting;
	private String barista;
	@SerializedName("hasAwards")
	private String hasAwards;
	@SerializedName("hasAlternateTypes")
	private String hasAlternateTypes;
	@SerializedName("toGoBuy")
	private boolean toGoBuy;
	@SerializedName("toBuy")
	private boolean toBuy;
	
	/**
	 * @return the coffeeAlternateTypes
	 */
	public CoffeeAlternateTypes getCoffeeAlternateTypes() {
		return coffeeAlternateTypes;
	}

	/**
	 * @param coffeeAlternateTypes the coffeeAlternateTypes to set
	 */
	public void setCoffeeAlternateTypes(CoffeeAlternateTypes coffeeAlternateTypes) {
		this.coffeeAlternateTypes = coffeeAlternateTypes;
	}

	/**
	 * @return the tnt
	 */
	public boolean isTnt() {
		return tnt;
	}

	/**
	 * @param tnt the tnt to set
	 */
	public void setTnt(boolean tnt) {
		this.tnt = tnt;
	}

	/**
	 * @return the tasting
	 */
	public boolean isTasting() {
		return tasting;
	}

	/**
	 * @param tasting the tasting to set
	 */
	public void setTasting(boolean tasting) {
		this.tasting = tasting;
	}

	/**
	 * @return the roasting
	 */
	public boolean isRoasting() {
		return roasting;
	}

	/**
	 * @param roasting the roasting to set
	 */
	public void setRoasting(boolean roasting) {
		this.roasting = roasting;
	}

	/**
	 * @return the barista
	 */
	public String getBarista() {
		return barista;
	}

	/**
	 * @param barista the barista to set
	 */
	public void setBarista(String barista) {
		this.barista = barista;
	}

	/**
	 * @return the hasAwards
	 */
	public String getHasAwards() {
		return hasAwards;
	}

	/**
	 * @return the hasAwards
	 */
	public boolean hasAwards() {
		if (this.hasAwards != null && this.hasAwards.equals("1")) {
			return true;
		}
		return false;
	}
	
	/**
	 * @param hasAwards the hasAwards to set
	 */
	public void setHasAwards(String hasAwards) {
		this.hasAwards = hasAwards;
	}

	/**
	 * @return the hasAlternateTypes
	 */
	public String getHasAlternateTypes() {
		return hasAlternateTypes;
	}

	/**
	 * @param hasAlternateTypes the hasAlternateTypes to set
	 */
	public void setHasAlternateTypes(String hasAlternateTypes) {
		this.hasAlternateTypes = hasAlternateTypes;
	}

	/**
	 * @return the hasAlternateTypes
	 */
	public boolean hasAlternateTypes() {
		if (this.hasAlternateTypes != null && this.hasAlternateTypes.equals("1")) {
			return true;
		}
		return false;
	}
	
	/**
	 * @return the toGoBuy
	 */
	public boolean isToGoBuy() {
		return toGoBuy;
	}

	/**
	 * @param toGoBuy the toGoBuy to set
	 */
	public void setToGoBuy(boolean toGoBuy) {
		this.toGoBuy = toGoBuy;
	}

	/**
	 * @return the toBuy
	 */
	public boolean isToBuy() {
		return toBuy;
	}

	/**
	 * @param toBuy the toBuy to set
	 */
	public void setToBuy(boolean toBuy) {
		this.toBuy = toBuy;
	}

	/******************************************/
    /********** Parcelable interface **********/
    /******************************************/

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
    	out.writeParcelable(this.getCoffeeAlternateTypes(), flags);
    	out.writeByte((byte) (this.isTnt() ? 1 : 0));
    	out.writeByte((byte) (this.isTasting() ? 1 : 0));
    	out.writeByte((byte) (this.isRoasting() ? 1 : 0));
    	out.writeString(this.getBarista());
    	out.writeString(this.getHasAwards());
    	out.writeString(this.getHasAlternateTypes());
    	out.writeByte((byte) (this.isToGoBuy() ? 1 : 0));
    	out.writeByte((byte) (this.isToBuy() ? 1 : 0));
    }

    private static Coffee readFromParcel(Parcel in) {
    	Coffee obj = new Coffee();
    	obj.setCoffeeAlternateTypes((CoffeeAlternateTypes)in.readParcelable(CoffeeAlternateTypes.class.getClassLoader()));
    	obj.setTnt(in.readByte() != 0);
    	obj.setTasting(in.readByte() != 0);
    	obj.setRoasting(in.readByte() != 0);
    	obj.setBarista(in.readString());
    	obj.setHasAwards(in.readString());
    	obj.setHasAlternateTypes(in.readString());
    	obj.setToGoBuy(in.readByte() != 0);
    	obj.setToBuy(in.readByte() != 0);
    	return obj;
    }

	public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
		public Coffee createFromParcel(Parcel in) {
			return readFromParcel(in);
		}

		public Coffee[] newArray(int size) {
			return new Coffee[size];
		}
	};
	
}
