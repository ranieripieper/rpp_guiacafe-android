package br.com.cafeeditora.guiadecafeterias.model;

import android.os.Parcel;
import android.os.Parcelable;

public class CoffeeAlternateTypes implements Parcelable {

	private boolean decaffeinated;

	/**
	 * @return the decaffeinated
	 */
	public boolean isDecaffeinated() {
		return decaffeinated;
	}

	/**
	 * @param decaffeinated the decaffeinated to set
	 */
	public void setDecaffeinated(boolean decaffeinated) {
		this.decaffeinated = decaffeinated;
	}
	
	/******************************************/
    /********** Parcelable interface **********/
    /******************************************/

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
    	out.writeByte((byte) (this.isDecaffeinated() ? 1 : 0));
    }

    private static CoffeeAlternateTypes readFromParcel(Parcel in) {
    	CoffeeAlternateTypes obj = new CoffeeAlternateTypes();
    	obj.setDecaffeinated(in.readByte() != 0);
        return obj;
    }

	public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
		public CoffeeAlternateTypes createFromParcel(Parcel in) {
			return readFromParcel(in);
		}

		public CoffeeAlternateTypes[] newArray(int size) {
			return new CoffeeAlternateTypes[size];
		}
	};
}
