package br.com.cafeeditora.guiadecafeterias.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Contact implements Parcelable {

	private String email;
	private String phone;
	private String website;
	
	public Contact() {}
	
	public Contact(String email, String phone, String website) {
		super();
		this.email = email;
		this.phone = phone;
		this.website = website;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * @param phone the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * @return the website
	 */
	public String getWebsite() {
		return website;
	}

	/**
	 * @param website the website to set
	 */
	public void setWebsite(String website) {
		this.website = website;
	}

	/******************************************/
    /********** Parcelable interface **********/
    /******************************************/

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeString(this.email);
        out.writeString(this.phone);
        out.writeString(this.website);        
    }

    private static Contact readFromParcel(Parcel in) {
        return new Contact(in.readString(), in.readString(), in.readString());
    }

	public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
		public Contact createFromParcel(Parcel in) {
			return readFromParcel(in);
		}

		public Contact[] newArray(int size) {
			return new Contact[size];
		}
	};
}
