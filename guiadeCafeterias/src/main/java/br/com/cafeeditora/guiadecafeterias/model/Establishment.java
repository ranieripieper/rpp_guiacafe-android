package br.com.cafeeditora.guiadecafeterias.model;

import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;

public class Establishment implements Parcelable {

	@SerializedName("capacityHome")
	private String capacityHome;

	private String opening;
	private boolean courses;
	@SerializedName("liveMusic")
	private boolean liveMusic;
	@SerializedName("closeTouristPoint")
	private boolean closeTouristPoint;
	@SerializedName("closeMetroPoint")
	private boolean closeMetroPoint;
		
	@SerializedName("itemsHouse")
	private ItemsHouse itemsHouse;

	/**
	 * @return the capacityHome
	 */
	public String getCapacityHome() {
		return capacityHome;
	}

	/**
	 * @param capacityHome the capacityHome to set
	 */
	public void setCapacityHome(String capacityHome) {
		this.capacityHome = capacityHome;
	}
	
	/**
	 * @return the itemsHouse
	 */
	public ItemsHouse getItemsHouse() {
		return itemsHouse;
	}

	/**
	 * @param itemsHouse the itemsHouse to set
	 */
	public void setItemsHouse(ItemsHouse itemsHouse) {
		this.itemsHouse = itemsHouse;
	}

	/**
	 * @return the opening
	 */
	public String getOpening() {
		return opening;
	}

	/**
	 * @param opening the opening to set
	 */
	public void setOpening(String opening) {
		this.opening = opening;
	}

	/**
	 * @return the courses
	 */
	public boolean isCourses() {
		return courses;
	}

	/**
	 * @param courses the courses to set
	 */
	public void setCourses(boolean courses) {
		this.courses = courses;
	}

	/**
	 * @return the liveMusic
	 */
	public boolean isLiveMusic() {
		return liveMusic;
	}

	/**
	 * @param liveMusic the liveMusic to set
	 */
	public void setLiveMusic(boolean liveMusic) {
		this.liveMusic = liveMusic;
	}

	/**
	 * @return the closeTouristPoint
	 */
	public boolean isCloseTouristPoint() {
		return closeTouristPoint;
	}

	/**
	 * @param closeTouristPoint the closeTouristPoint to set
	 */
	public void setCloseTouristPoint(boolean closeTouristPoint) {
		this.closeTouristPoint = closeTouristPoint;
	}

	/**
	 * @return the closeMetroPoint
	 */
	public boolean isCloseMetroPoint() {
		return closeMetroPoint;
	}

	/**
	 * @param closeMetroPoint the closeMetroPoint to set
	 */
	public void setCloseMetroPoint(boolean closeMetroPoint) {
		this.closeMetroPoint = closeMetroPoint;
	}

	/******************************************/
    /********** Parcelable interface **********/
    /******************************************/

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeString(this.capacityHome);
        out.writeString(this.opening);
        out.writeByte((byte) (this.isCloseMetroPoint() ? 1 : 0));
        out.writeByte((byte) (this.isCloseTouristPoint() ? 1 : 0));
        out.writeByte((byte) (this.isCourses() ? 1 : 0));
        out.writeByte((byte) (this.isLiveMusic() ? 1 : 0));
        out.writeParcelable(this.getItemsHouse(), flags);
    }

    private static Establishment readFromParcel(Parcel in) {
    	Establishment obj = new Establishment();
    	obj.setCapacityHome(in.readString());
    	obj.setOpening(in.readString());
    	obj.setCloseMetroPoint(in.readByte() != 0);
    	obj.setCloseTouristPoint(in.readByte() != 0);
    	obj.setCourses(in.readByte() != 0);
    	obj.setLiveMusic(in.readByte() != 0);
    	obj.setItemsHouse((ItemsHouse)in.readParcelable(ItemsHouse.class.getClassLoader()));
        return obj;
    }

	public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
		public Establishment createFromParcel(Parcel in) {
			return readFromParcel(in);
		}

		public Establishment[] newArray(int size) {
			return new Establishment[size];
		}
	};
}
