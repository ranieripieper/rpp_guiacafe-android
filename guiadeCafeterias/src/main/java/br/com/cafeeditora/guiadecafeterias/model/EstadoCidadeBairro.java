package br.com.cafeeditora.guiadecafeterias.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by ranipieper on 10/1/15.
 */
public class EstadoCidadeBairro implements Parcelable {

    private boolean success;
    private boolean error;
    private String[] data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String[] getData() {
        return data;
    }

    public void setData(String[] data) {
        this.data = data;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte(success ? (byte) 1 : (byte) 0);
        dest.writeByte(error ? (byte) 1 : (byte) 0);
        dest.writeStringArray(this.data);
    }

    public EstadoCidadeBairro() {
    }

    protected EstadoCidadeBairro(Parcel in) {
        this.success = in.readByte() != 0;
        this.error = in.readByte() != 0;
        this.data = in.createStringArray();
    }

    public static final Creator<EstadoCidadeBairro> CREATOR = new Creator<EstadoCidadeBairro>() {
        public EstadoCidadeBairro createFromParcel(Parcel source) {
            return new EstadoCidadeBairro(source);
        }

        public EstadoCidadeBairro[] newArray(int size) {
            return new EstadoCidadeBairro[size];
        }
    };
}
