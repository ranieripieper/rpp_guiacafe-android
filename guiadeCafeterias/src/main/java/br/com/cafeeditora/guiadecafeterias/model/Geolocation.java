package br.com.cafeeditora.guiadecafeterias.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Geolocation implements Parcelable {

	private Double longitude;
	private Double latitude;
	
	public Geolocation() {}
	
	public Geolocation(Double longitude, Double latitude) {
		super();
		this.longitude = longitude;
		this.latitude = latitude;
	}
	public Double getLongitude() {
		return longitude;
	}
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	public Double getLatitude() {
		return latitude;
	}
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	
	/******************************************/
    /********** Parcelable interface **********/
    /******************************************/

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeDouble(this.longitude);
        out.writeDouble(this.latitude);
        
    }

    private static Geolocation readFromParcel(Parcel in) {
        Double lng = in.readDouble();
        Double lat = in.readDouble();
        return new Geolocation(lng, lat);
    }

	public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
		public Geolocation createFromParcel(Parcel in) {
			return readFromParcel(in);
		}

		public Geolocation[] newArray(int size) {
			return new Geolocation[size];
		}
	};
}
