package br.com.cafeeditora.guiadecafeterias.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Informations implements Parcelable {
	
	private String serve;
	private String extraction;
	@SerializedName("toDrink") 
	private String toDrink;
	@SerializedName("toEat") 
	private String toEat;
	@SerializedName("strongPoints") 
	private String strongPoints;
	
	/**
	 * @return the serve
	 */
	public String getServe() {
		return serve;
	}
	/**
	 * @param serve the serve to set
	 */
	public void setServe(String serve) {
		this.serve = serve;
	}
	/**
	 * @return the extraction
	 */
	public String getExtraction() {
		return extraction;
	}
	/**
	 * @param extraction the extraction to set
	 */
	public void setExtraction(String extraction) {
		this.extraction = extraction;
	}
	/**
	 * @return the toDrink
	 */
	public String getToDrink() {
		return toDrink;
	}
	/**
	 * @param toDrink the toDrink to set
	 */
	public void setToDrink(String toDrink) {
		this.toDrink = toDrink;
	}
	/**
	 * @return the toEat
	 */
	public String getToEat() {
		return toEat;
	}
	/**
	 * @param toEat the toEat to set
	 */
	public void setToEat(String toEat) {
		this.toEat = toEat;
	}
	/**
	 * @return the strongPoints
	 */
	public String getStrongPoints() {
		return strongPoints;
	}
	/**
	 * @param strongPoints the strongPoints to set
	 */
	public void setStrongPoints(String strongPoints) {
		this.strongPoints = strongPoints;
	}
	
	/******************************************/
    /********** Parcelable interface **********/
    /******************************************/

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeString(this.extraction);
        out.writeString(this.serve);
        out.writeString(this.strongPoints);
        out.writeString(this.toDrink);
        out.writeString(this.toEat);
        
    }

    private static Informations readFromParcel(Parcel in) {
    	Informations obj = new Informations();
    	obj.setExtraction(in.readString());
    	obj.setServe(in.readString());
    	obj.setStrongPoints(in.readString());
    	obj.setToDrink(in.readString());
    	obj.setToEat(in.readString());
        return obj;
    }

	public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
		public Informations createFromParcel(Parcel in) {
			return readFromParcel(in);
		}

		public Informations[] newArray(int size) {
			return new Informations[size];
		}
	};
}
