package br.com.cafeeditora.guiadecafeterias.model;

import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;

public class ItemsHouse implements Parcelable {

	private boolean parking;
	private boolean accessibility;
	@SerializedName("compressedAir")
	private boolean compressedAir;
	private boolean wc;
	private boolean smoking;
	private boolean wifi;
	private boolean attested;
	@SerializedName("openAir")
	private boolean openAir;
	private boolean couch;
	private boolean franchisee;
	/**
	 * @return the parking
	 */
	public boolean isParking() {
		return parking;
	}
	/**
	 * @param parking the parking to set
	 */
	public void setParking(boolean parking) {
		this.parking = parking;
	}
	/**
	 * @return the accessibility
	 */
	public boolean isAccessibility() {
		return accessibility;
	}
	/**
	 * @param accessibility the accessibility to set
	 */
	public void setAccessibility(boolean accessibility) {
		this.accessibility = accessibility;
	}
	/**
	 * @return the compressedAir
	 */
	public boolean isCompressedAir() {
		return compressedAir;
	}
	/**
	 * @param compressedAir the compressedAir to set
	 */
	public void setCompressedAir(boolean compressedAir) {
		this.compressedAir = compressedAir;
	}
	/**
	 * @return the wc
	 */
	public boolean isWc() {
		return wc;
	}
	/**
	 * @param wc the wc to set
	 */
	public void setWc(boolean wc) {
		this.wc = wc;
	}
	/**
	 * @return the smoking
	 */
	public boolean isSmoking() {
		return smoking;
	}
	/**
	 * @param smoking the smoking to set
	 */
	public void setSmoking(boolean smoking) {
		this.smoking = smoking;
	}
	/**
	 * @return the wifi
	 */
	public boolean isWifi() {
		return wifi;
	}
	/**
	 * @param wifi the wifi to set
	 */
	public void setWifi(boolean wifi) {
		this.wifi = wifi;
	}
	/**
	 * @return the attested
	 */
	public boolean isAttested() {
		return attested;
	}
	/**
	 * @param attested the attested to set
	 */
	public void setAttested(boolean attested) {
		this.attested = attested;
	}
	/**
	 * @return the openAir
	 */
	public boolean isOpenAir() {
		return openAir;
	}
	/**
	 * @param openAir the openAir to set
	 */
	public void setOpenAir(boolean openAir) {
		this.openAir = openAir;
	}
	/**
	 * @return the couch
	 */
	public boolean isCouch() {
		return couch;
	}
	/**
	 * @param couch the couch to set
	 */
	public void setCouch(boolean couch) {
		this.couch = couch;
	}
	/**
	 * @return the franchisee
	 */
	public boolean isFranchisee() {
		return franchisee;
	}
	/**
	 * @param franchisee the franchisee to set
	 */
	public void setFranchisee(boolean franchisee) {
		this.franchisee = franchisee;
	}
	
	/******************************************/
    /********** Parcelable interface **********/
    /******************************************/

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
    	 out.writeByte((byte) (this.isAccessibility() ? 1 : 0));
    	 out.writeByte((byte) (this.isAttested() ? 1 : 0));
    	 out.writeByte((byte) (this.isCompressedAir() ? 1 : 0));
    	 out.writeByte((byte) (this.isCouch() ? 1 : 0));
    	 out.writeByte((byte) (this.isFranchisee() ? 1 : 0));
    	 out.writeByte((byte) (this.isOpenAir() ? 1 : 0));
    	 out.writeByte((byte) (this.isParking() ? 1 : 0));
    	 out.writeByte((byte) (this.isSmoking() ? 1 : 0));
    	 out.writeByte((byte) (this.isWc() ? 1 : 0));
    	 out.writeByte((byte) (this.isWifi() ? 1 : 0));
    }

    private static ItemsHouse readFromParcel(Parcel in) {
    	ItemsHouse obj = new ItemsHouse();
    	obj.setAccessibility(in.readByte() != 0);
    	obj.setAttested(in.readByte() != 0);
    	obj.setCompressedAir(in.readByte() != 0);
    	obj.setCouch(in.readByte() != 0);
    	obj.setFranchisee(in.readByte() != 0);
    	obj.setOpenAir(in.readByte() != 0);
    	obj.setParking(in.readByte() != 0);
    	obj.setSmoking(in.readByte() != 0);
    	obj.setWc(in.readByte() != 0);
    	obj.setWifi(in.readByte() != 0);
        return obj;
    }

	public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
		public ItemsHouse createFromParcel(Parcel in) {
			return readFromParcel(in);
		}

		public ItemsHouse[] newArray(int size) {
			return new ItemsHouse[size];
		}
	};
}
