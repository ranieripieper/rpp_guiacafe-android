package br.com.cafeeditora.guiadecafeterias.model;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class Pagination implements Serializable {

	private static final long serialVersionUID = -8985480628883593843L;
	
	@SerializedName("totalPages")
	private  Integer totalPages;
	@SerializedName("currentPage")
	private  Integer currentPage;
	public Integer getTotalPages() {
		return totalPages;
	}
	public void setTotalPages(Integer totalPages) {
		this.totalPages = totalPages;
	}
	public Integer getCurrentPage() {
		return currentPage;
	}
	public void setCurrentPage(Integer currentPage) {
		this.currentPage = currentPage;
	}
	
	
}
