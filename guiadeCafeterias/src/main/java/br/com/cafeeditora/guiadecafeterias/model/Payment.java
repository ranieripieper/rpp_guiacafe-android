package br.com.cafeeditora.guiadecafeterias.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Payment implements Parcelable {

	private boolean check;

	/**
	 * @return the check
	 */
	public boolean isCheck() {
		return check;
	}

	/**
	 * @param check the check to set
	 */
	public void setCheck(boolean check) {
		this.check = check;
	}

	/******************************************/
    /********** Parcelable interface **********/
    /******************************************/

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
    	out.writeByte((byte) (this.check ? 1 : 0));
    }

    private static Payment readFromParcel(Parcel in) {
    	Payment obj = new Payment();
    	obj.setCheck(in.readByte() != 0);
        return obj;
    }

	public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
		public Payment createFromParcel(Parcel in) {
			return readFromParcel(in);
		}

		public Payment[] newArray(int size) {
			return new Payment[size];
		}
	};
}
