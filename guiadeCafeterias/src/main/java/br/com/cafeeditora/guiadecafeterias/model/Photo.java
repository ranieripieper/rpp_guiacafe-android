package br.com.cafeeditora.guiadecafeterias.model;

import java.io.Serializable;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import br.com.cafeeditora.guiadecafeterias.util.Constants;

public class Photo implements Serializable, Parcelable {

	private static final long serialVersionUID = 9090874498593396850L;

	private String name;
	private String url;

	public Photo() {
	}
	
	public Photo(String name, String url) {
		super();
		this.name = name;
		this.url = url;
	}
	
	public static String getUrlPhoto(Photo ph) {
		if (ph != null && !TextUtils.isEmpty(ph.getUrl())) {
			return Constants.DETAIL_PHOTO.replace("{src}", ph.getUrl());
		}
		return "";
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
	/******************************************/
    /********** Parcelable interface **********/
    /******************************************/

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeString(this.name);
        out.writeString(this.url);
        
    }

    private static Photo readFromParcel(Parcel in) {
        String name = in.readString();
        String url = in.readString();
        return new Photo(name, url);
    }

	public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
		public Photo createFromParcel(Parcel in) {
			return readFromParcel(in);
		}

		public Photo[] newArray(int size) {
			return new Photo[size];
		}
	};
}
