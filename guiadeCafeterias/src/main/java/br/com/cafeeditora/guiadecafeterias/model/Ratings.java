package br.com.cafeeditora.guiadecafeterias.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Ratings implements Parcelable {

	private int total = 0;
	private float average = 0.0f;
	/**
	 * @return the total
	 */
	public int getTotal() {
		return total;
	}
	/**
	 * @param total the total to set
	 */
	public void setTotal(int total) {
		this.total = total;
	}
	/**
	 * @return the average
	 */
	public float getAverage() {
		return average;
	}
	/**
	 * @param average the average to set
	 */
	public void setAverage(float average) {
		this.average = average;
	}

	/******************************************/
    /********** Parcelable interface **********/
    /******************************************/

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeFloat(this.average);
        out.writeInt(this.total);
    }

    private static Ratings readFromParcel(Parcel in) {
    	Ratings obj = new Ratings();
    	obj.setAverage(in.readFloat());
    	obj.setTotal(in.readInt());
        return obj;
    }

	public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
		public Ratings createFromParcel(Parcel in) {
			return readFromParcel(in);
		}

		public Ratings[] newArray(int size) {
			return new Ratings[size];
		}
	};
	
	
}
