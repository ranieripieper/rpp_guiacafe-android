package br.com.cafeeditora.guiadecafeterias.model;

import java.io.Serializable;

public class Related implements Serializable {

	private static final long serialVersionUID = 8755186359858292353L;
	
	private Integer results;
	private Integer found;
	private Integer all;
	private Pagination pagination;
	public Integer getResults() {
		return results;
	}
	public void setResults(Integer results) {
		this.results = results;
	}
	public Integer getFound() {
		return found;
	}
	public void setFound(Integer found) {
		this.found = found;
	}
	public Integer getAll() {
		return all;
	}
	public void setAll(Integer all) {
		this.all = all;
	}
	public Pagination getPagination() {
		return pagination;
	}
	public void setPagination(Pagination pagination) {
		this.pagination = pagination;
	}
	
	
}
