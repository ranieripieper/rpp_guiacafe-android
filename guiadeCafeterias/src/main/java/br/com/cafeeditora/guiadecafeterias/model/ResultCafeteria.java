package br.com.cafeeditora.guiadecafeterias.model;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class ResultCafeteria implements Serializable {

	private static final long serialVersionUID = -790322045209903420L;

	private boolean success;
	private boolean error;
	//private Related related;
	
	@SerializedName("data")
	private Cafeteria[] cafeterias;
	
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public boolean isError() {
		return error;
	}
	public void setError(boolean error) {
		this.error = error;
	}
	/*public Related getRelated() {
		return related;
	}
	public void setRelated(Related related) {
		this.related = related;
	}*/
	public Cafeteria[] getCafeterias() {
		return cafeterias;
	}
	public void setCafeterias(Cafeteria[] cafeterias) {
		this.cafeterias = cafeterias;
	}
	
}
