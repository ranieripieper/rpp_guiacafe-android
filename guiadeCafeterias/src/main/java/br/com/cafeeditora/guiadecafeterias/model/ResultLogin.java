package br.com.cafeeditora.guiadecafeterias.model;

public class ResultLogin {

	public static final String PARAM_NAME = "name";
	public static final String PARAM_SOURCE = "source";
	public static final String PARAM_EMAIL = "email";
	public static final String PARAM_TOKEN = "sourceToken";
	public static final String PARAM_FACEBOOK_ID = "facebookId";
	
	private boolean success;
	private LoginData[] data;
	
	/**
	 * @return the success
	 */
	public boolean isSuccess() {
		return success;
	}
	/**
	 * @param success the success to set
	 */
	public void setSuccess(boolean success) {
		this.success = success;
	}
	/**
	 * @return the data
	 */
	public LoginData[] getData() {
		return data;
	}
	/**
	 * @param data the data to set
	 */
	public void setData(LoginData[] data) {
		this.data = data;
	}	
	
	
}
