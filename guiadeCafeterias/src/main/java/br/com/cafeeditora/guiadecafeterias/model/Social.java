package br.com.cafeeditora.guiadecafeterias.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Social implements Parcelable {

	private String facebook;

	/**
	 * @return the facebook
	 */
	public String getFacebook() {
		return facebook;
	}

	/**
	 * @param facebook the facebook to set
	 */
	public void setFacebook(String facebook) {
		this.facebook = facebook;
	}
	
	/******************************************/
    /********** Parcelable interface **********/
    /******************************************/

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
    	out.writeString(this.getFacebook());
    }

    private static Social readFromParcel(Parcel in) {
    	Social obj = new Social();
    	obj.setFacebook(in.readString());
    	return obj;
    }

	public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
		public Social createFromParcel(Parcel in) {
			return readFromParcel(in);
		}

		public Social[] newArray(int size) {
			return new Social[size];
		}
	};
}
