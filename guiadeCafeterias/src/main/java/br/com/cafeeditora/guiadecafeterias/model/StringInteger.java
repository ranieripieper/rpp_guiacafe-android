package br.com.cafeeditora.guiadecafeterias.model;

public class StringInteger implements Comparable<StringInteger> {

	private int order;
	private String str;
	private Integer nr;
	
	public StringInteger() {}
	
	public StringInteger(int order, String str, Integer nr) {
		super();
		this.order = order;
		this.str = str;
		this.nr = nr;
	}
	/**
	 * @return the str
	 */
	public String getStr() {
		return str;
	}
	/**
	 * @param str the str to set
	 */
	public void setStr(String str) {
		this.str = str;
	}
	/**
	 * @return the nr
	 */
	public Integer getNr() {
		return nr;
	}
	/**
	 * @param nr the nr to set
	 */
	public void setNr(Integer nr) {
		this.nr = nr;
	}

	/**
	 * @return the order
	 */
	public int getOrder() {
		return order;
	}

	/**
	 * @param order the order to set
	 */
	public void setOrder(int order) {
		this.order = order;
	}
	
	@Override
	public int compareTo(StringInteger another) {
		return this.order - another.order;
	}
	
}
