package br.com.cafeeditora.guiadecafeterias.model;

import android.os.Parcel;
import android.os.Parcelable;

public class UserData implements Parcelable {

	private boolean favorite = false;
	private float rating = 0.0f;
	
	/**
	 * @return the favorite
	 */
	public boolean isFavorite() {
		return favorite;
	}

	/**
	 * @param favorite the favorite to set
	 */
	public void setFavorite(boolean favorite) {
		this.favorite = favorite;
	}

	/**
	 * @return the rating
	 */
	public float getRating() {
		return rating;
	}

	/**
	 * @param rating the rating to set
	 */
	public void setRating(float rating) {
		this.rating = rating;
	}

	/******************************************/
    /********** Parcelable interface **********/
    /******************************************/

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeFloat(this.rating);
        out.writeByte((byte) (this.isFavorite() ? 1 : 0));
    }

    private static UserData readFromParcel(Parcel in) {
    	UserData obj = new UserData();
    	obj.setRating(in.readFloat());
    	obj.setFavorite(in.readByte() != 0);
        return obj;
    }

	public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
		public UserData createFromParcel(Parcel in) {
			return readFromParcel(in);
		}

		public UserData[] newArray(int size) {
			return new UserData[size];
		}
	};
}
