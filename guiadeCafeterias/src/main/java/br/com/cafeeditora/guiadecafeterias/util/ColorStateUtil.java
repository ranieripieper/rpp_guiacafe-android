package br.com.cafeeditora.guiadecafeterias.util;

import android.content.Context;
import br.com.cafeeditora.guiadecafeterias.R;

import com.doisdoissete.android.util.ddsutil.StringUtil;
import com.doisdoissete.android.util.ddsutil.view.ResourceUtil;

public class ColorStateUtil {

	
	public static int getMapIcon(String state) {
		
		state = StringUtil.getNormalizeStr(state);
		if ("acre".equals(state)) {
			return R.drawable.pin_map_acre;
		} else if ("alagoas".equals(state)) {
			return R.drawable.pin_map_alagoas;
		} else if ("amapa".equals(state)) {
			return R.drawable.pin_map_amapa;
		} else if ("amazonas".equals(state)) {
			return R.drawable.pin_map_amazonas;
		} else if ("bahia".equals(state)) {
			return R.drawable.pin_map_bahia;
		} else if ("ceara".equals(state)) {
			return R.drawable.pin_map_ceara;
		} else if ("df".equals(state) || "distrito_federal".equals(state)) {
			return R.drawable.pin_map_df;
		} else if ("espirito_santo".equals(state)) {
			return R.drawable.pin_map_espirito_santo;
		} else if ("goias".equals(state)) {
			return R.drawable.pin_map_goias;
		} else if ("maranhao".equals(state)) {
			return R.drawable.pin_map_maranhao;
		} else if ("mato_grosso".equals(state)) {
			return R.drawable.pin_map_mato_grosso;
		} else if ("mato_grosso_do_sul".equals(state)) {
			return R.drawable.pin_map_mato_grosso_do_sul;
		} else if ("minas_gerais".equals(state)) {
			return R.drawable.pin_map_minas_gerais;
		} else if ("para".equals(state)) {
			return R.drawable.pin_map_para;
		} else if ("paraiba".equals(state)) {
			return R.drawable.pin_map_paraiba;
		} else if ("parana".equals(state)) {
			return R.drawable.pin_map_parana;
		} else if ("pernambuco".equals(state)) {
			return R.drawable.pin_map_pernambuco;
		} else if ("piaui".equals(state)) {
			return R.drawable.pin_map_piaui;
		} else if ("rio_grande_do_norte".equals(state)) {
			return R.drawable.pin_map_rio_grande_do_norte;
		} else if ("rio_grande_do_sul".equals(state)) {
			return R.drawable.pin_map_rio_grande_do_sul;
		} else if ("rj".equals(state) || "rio_de_janeiro".equals(state)) {
			return R.drawable.pin_map_rj;
		} else if ("rondonia".equals(state)) {
			return R.drawable.pin_map_rondonia;
		} else if ("roraima".equals(state)) {
			return R.drawable.pin_map_roraima;
		} else if ("santa_catarina".equals(state)) {
			return R.drawable.pin_map_santa_catarina;
		} else if ("sergipe".equals(state)) {
			return R.drawable.pin_map_sergipe;
		} else if ("sp".equals(state) || "sao_paulo".equals(state)) {
			return R.drawable.pin_map_sp;
		} else if ("tocantins".equals(state)) {
			return R.drawable.pin_map_tocantins;
		}
		
		return R.drawable.pin_map_tocantins;
	}
	
	public static int getPinByState(String state) {
		
		state = StringUtil.getNormalizeStr(state);
		
		if ("acre".equals(state)) {
			return R.drawable.icn_pin_acre;
		} else if ("alagoas".equals(state)) {
			return R.drawable.icn_pin_alagoas;
		} else if ("amapa".equals(state)) {
			return R.drawable.icn_pin_amapa;
		} else if ("amazonas".equals(state)) {
			return R.drawable.icn_pin_amazonas;
		} else if ("bahia".equals(state)) {
			return R.drawable.icn_pin_bahia;
		} else if ("ceara".equals(state)) {
			return R.drawable.icn_pin_ceara;
		} else if ("df".equals(state) || "distrito_federal".equals(state)) {
			return R.drawable.icn_pin_df;
		} else if ("espirito_santo".equals(state)) {
			return R.drawable.icn_pin_espirito_santo;
		} else if ("goias".equals(state)) {
			return R.drawable.icn_pin_goias;
		} else if ("maranhao".equals(state)) {
			return R.drawable.icn_pin_maranhao;
		} else if ("mato_grosso".equals(state)) {
			return R.drawable.icn_pin_mato_grosso;
		} else if ("mato_grosso_do_sul".equals(state)) {
			return R.drawable.icn_pin_mato_grosso_do_sul;
		} else if ("minas_gerais".equals(state)) {
			return R.drawable.icn_pin_minas_gerais;
		} else if ("para".equals(state)) {
			return R.drawable.icn_pin_para;
		} else if ("paraiba".equals(state)) {
			return R.drawable.icn_pin_paraiba;
		} else if ("parana".equals(state)) {
			return R.drawable.icn_pin_parana;
		} else if ("pernambuco".equals(state)) {
			return R.drawable.icn_pin_pernambuco;
		} else if ("piaui".equals(state)) {
			return R.drawable.icn_pin_piaui;
		} else if ("rio_grande_do_norte".equals(state)) {
			return R.drawable.icn_pin_rio_grande_do_norte;
		} else if ("rio_grande_do_sul".equals(state)) {
			return R.drawable.icn_pin_rio_grande_do_sul;
		} else if ("rj".equals(state) || "rio_de_janeiro".equals(state)) {
			return R.drawable.icn_pin_rj;
		} else if ("rondonia".equals(state)) {
			return R.drawable.icn_pin_rondonia;
		} else if ("roraima".equals(state)) {
			return R.drawable.icn_pin_roraima;
		} else if ("santa_catarina".equals(state)) {
			return R.drawable.icn_pin_santa_catarina;
		} else if ("sergipe".equals(state)) {
			return R.drawable.icn_pin_sergipe;
		} else if ("sp".equals(state) || "sao_paulo".equals(state)) {
			return R.drawable.icn_pin_sp;
		} else if ("tocantins".equals(state)) {
			return R.drawable.icn_pin_tocantins;
		}
		
		return R.drawable.icn_pin_tocantins;
	}
	
	public static int getColorByState(String state, Context mContext) {
		state = StringUtil.getNormalizeStr(state);
		return ResourceUtil.getColorId(state, mContext);
	}
	
	public static int getBgColorByState(String state, Context mContext) {
		state = StringUtil.getNormalizeStr(state);
		return ResourceUtil.getColorId("bg_" + state, mContext);
	}
}
