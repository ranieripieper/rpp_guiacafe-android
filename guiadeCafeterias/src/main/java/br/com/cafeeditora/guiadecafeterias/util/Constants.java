package br.com.cafeeditora.guiadecafeterias.util;

import br.com.cafeeditora.guiadecafeterias.model.Cafeteria;

/**
 * Classe de constants
 * @author ranieripieper
 *
 */
public class Constants {
	
	public static final String PARAM_PAGE = "page";
	public static final String PARAM_GEOLOCATION = "geolocation[]";
	public static final String PARAM_GEOLOCATION_LAT = "lat";
	public static final String PARAM_GEOLOCATION_LNG = "lng";
	public static final String PARAM_ORDER_BY = "orderBy";
	public static final String PARAM_ORDER = "order";
	public static final String PARAM_STATE = "state";
	public static final String PARAM_CITY = "city";
    public static final String PARAM_DISTRICT = "district";
	
	public static final String URL_SHARE = "https://play.google.com/store/apps/details?id=br.com.cafeeditora.guiadecafeterias";
	
	public static final String TAG_DEBUG = "#dds_giuacafeterias";
	public static final String SERVICE_HOST = "http://guiadecafeterias.com.br";
	public static final int DIAS_CACHE = 10;
	public static final String TOTAL_ITENS_PAGE = "25";
	
	public static final String PARAM_FILTERS = "filters[]";
	public static final String PARAM_TEXT_SEARCH = "search";
	
	public static final String QUERY_PATH_GEOLOCATION = String.format("%s/api/places/query.json?%s={%s}&%s={%s}&perPage=%s&%s={%s}&%s={%s}&%s={%s}&%s={%s}", SERVICE_HOST, Cafeteria.PARAM_ACCESS_TOKEN, Cafeteria.PARAM_ACCESS_TOKEN, 
																PARAM_PAGE, PARAM_PAGE, TOTAL_ITENS_PAGE, 
																PARAM_GEOLOCATION, PARAM_GEOLOCATION_LNG, PARAM_GEOLOCATION, PARAM_GEOLOCATION_LAT,
																PARAM_ORDER_BY, PARAM_ORDER_BY,
																PARAM_ORDER, PARAM_ORDER);
	public static final String QUERY_PATH_GEOLOCATION_WITHOUT_LOCATION = String.format("%s/api/places/query.json?%s={%s}&%s={%s}&perPage=%s&%s={%s}&%s={%s}", SERVICE_HOST, Cafeteria.PARAM_ACCESS_TOKEN, Cafeteria.PARAM_ACCESS_TOKEN,
			PARAM_PAGE, PARAM_PAGE, TOTAL_ITENS_PAGE,
			PARAM_ORDER_BY, PARAM_ORDER_BY,
			PARAM_ORDER, PARAM_ORDER);

	public static final String QUERY_PATH = String.format("%s/api/places/query.json?%s={%s}&%s={%s}&perPage=%s", SERVICE_HOST, Cafeteria.PARAM_ACCESS_TOKEN, Cafeteria.PARAM_ACCESS_TOKEN, PARAM_PAGE, PARAM_PAGE, TOTAL_ITENS_PAGE);
	public static final String QUERY_PATH_TOP_20 = String.format("%s/api/places/query.json?top20=1&%s={%s}&perPage=%s&%s={%s}&%s={%s}&%s={%s}", SERVICE_HOST, PARAM_PAGE, PARAM_PAGE, TOTAL_ITENS_PAGE, Cafeteria.PARAM_ACCESS_TOKEN, Cafeteria.PARAM_ACCESS_TOKEN, Constants.PARAM_ORDER, Constants.PARAM_ORDER, Constants.PARAM_ORDER_BY, Constants.PARAM_ORDER_BY);
	public static final String QUERY_PATH_CAFETERIA_REVELACAO = String.format("%s/api/places/query.json?cafeteriaRevelacao=1&%s={%s}&perPage=%s&%s={%s}&%s={%s}&%s={%s}", SERVICE_HOST, PARAM_PAGE, PARAM_PAGE, TOTAL_ITENS_PAGE, Cafeteria.PARAM_ACCESS_TOKEN, Cafeteria.PARAM_ACCESS_TOKEN, Constants.PARAM_ORDER, Constants.PARAM_ORDER, Constants.PARAM_ORDER_BY, Constants.PARAM_ORDER_BY);
	public static final String QUERY_PATH_ESPRESSO_INDICA = String.format("%s/api/places/query.json?espressoIndica20=1&%s={%s}&perPage=%s&%s={%s}", SERVICE_HOST, PARAM_PAGE, PARAM_PAGE, TOTAL_ITENS_PAGE, Cafeteria.PARAM_ACCESS_TOKEN, Cafeteria.PARAM_ACCESS_TOKEN);
	public static final String FAVORITOS = String.format("%s/api/favorites/list.json?%s={%s}&perPage=%s&%s={%s}", SERVICE_HOST, PARAM_PAGE, PARAM_PAGE, TOTAL_ITENS_PAGE, Cafeteria.PARAM_ACCESS_TOKEN, Cafeteria.PARAM_ACCESS_TOKEN);
	
	public static final String LOGIN = String.format("%s/api/oauth/connect.json", SERVICE_HOST);
	public static final String FAVORITAR = String.format("%s/api/favorites/%s/add.json", SERVICE_HOST, Cafeteria.PARAM_ID);
	public static final String DESFAVORITAR = String.format("%s/api/favorites/%s/remove.json", SERVICE_HOST, Cafeteria.PARAM_ID);
	public static final String AVALIAR = String.format("%s/api/places/%s/rate.json", SERVICE_HOST, Cafeteria.PARAM_ID);
	public static final String THUMB_HOME_PHOTO = "http://guiadecafeterias.com.br/images/thumb.php?src={src}&w=460&h=352";
	public static final String DETAIL_PHOTO = "http://guiadecafeterias.com.br/images/thumb.php?src={src}&w=460&h=352";
	
	public static final String CAFETERIA = String.format("%s/api/places/%s.json?%s={%s}", SERVICE_HOST, Cafeteria.PARAM_ID, Cafeteria.PARAM_ACCESS_TOKEN, Cafeteria.PARAM_ACCESS_TOKEN);

	public static final String STATE_CITIES_DISTRICTS = String.format("%s/api/places/search/complete.json?%s={%s}&%s={%s}", SERVICE_HOST, PARAM_STATE, PARAM_STATE, PARAM_CITY, PARAM_CITY);



	public static final String GOOGLE_PLUS = "google";
	public static final String FACEBOOK = "facebook";
}
