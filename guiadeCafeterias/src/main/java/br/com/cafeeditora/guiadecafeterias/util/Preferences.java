package br.com.cafeeditora.guiadecafeterias.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class Preferences {

	private static final String PREFERENCES = Preferences.class + "";
	private static final String PREF_LAST_CLEAR_CACHE = "PREF_LAST_CLEAR_CACHE";
	private static final String PREF_TOKEN = "PREF_TOKEN";
	private static final String PREF_FAV_DESFAV = "PREF_FAV_DESFAV";
	private static final String PREF_ESTADOS = "PREF_ESTADOS";

	public static Date getLastClearCache(Context ctx) {
		SharedPreferences settings = ctx.getSharedPreferences(PREFERENCES, 0);
		long time = settings.getLong(PREF_LAST_CLEAR_CACHE, -1);
		if (time > 0) {
			return new Date(time);
		}
		return null;
	}
	
	public static void setLastClearCache(Context ctx, Date value) {
		SharedPreferences settings = ctx.getSharedPreferences(PREFERENCES, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putLong(PREF_LAST_CLEAR_CACHE, value.getTime());
		editor.commit();
	}
	
	public static String getToken(Context ctx) {
		SharedPreferences settings = ctx.getSharedPreferences(PREFERENCES, 0);
		String v = settings.getString(PREF_TOKEN, "");
		return v;
	}
	
	public static void setToken(Context ctx, String value) {
		SharedPreferences settings = ctx.getSharedPreferences(PREFERENCES, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(PREF_TOKEN, value);
		editor.commit();
	}
	
	public static boolean userIsLogado(Context ctx) {
		if (TextUtils.isEmpty(getToken(ctx))) {
			return false;
		}
		return true;
	}

	
	public static boolean isFavDesfavClicked(Context ctx) {
		SharedPreferences settings = ctx.getSharedPreferences(PREFERENCES, 0);
		boolean v = settings.getBoolean(PREF_FAV_DESFAV, false);
		return v;
	}
	
	public static void setFavDesfavClicked(Context ctx, boolean value) {
		SharedPreferences settings = ctx.getSharedPreferences(PREFERENCES, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putBoolean(PREF_FAV_DESFAV, value);
		editor.commit();
	}

	public static String[] getEstados(Context ctx) {
		SharedPreferences settings = ctx.getSharedPreferences(PREFERENCES, 0);
		Set<String> set = settings.getStringSet(PREF_ESTADOS, null);
		if (set == null || set.isEmpty()) {
			return null;
		}
		String[] res = new String[set.size()];
		return set.toArray(res);
	}

	public static void setEstados(Context ctx, String[] value) {

		SharedPreferences settings = ctx.getSharedPreferences(PREFERENCES, 0);
		SharedPreferences.Editor editor = settings.edit();

		Set<String> set = new HashSet();
		set.addAll(Arrays.asList(value));
		editor.putStringSet(PREF_ESTADOS, set);
		editor.commit();
		editor.commit();
	}
}
