package br.com.cafeeditora.guiadecafeterias.util;

import android.widget.ImageView;

public class RateUtil {

	private static float minRate = 0.25f;
	private static float maxRate = 0.75f;
	
	public static void setRate(ImageView[] imageView, float rate, int[] res) {
		
		for (int i=0; i < imageView.length; i++) {
			if (rate > i+maxRate) {
				imageView[i].setImageResource(res[2]);
			} else if (rate > i+minRate && rate <= i+maxRate) {
				imageView[i].setImageResource(res[1]);
			} else {
				imageView[i].setImageResource(res[0]);
			}
		}
	}
	
}
