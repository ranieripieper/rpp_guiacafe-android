package br.com.cafeeditora.guiadecafeterias.view.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import br.com.cafeeditora.guiadecafeterias.R;

public class AboutActivity extends BaseActivity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.about_activity);
	}

	
	public static void showActivity(Context ctx) {
		Intent it = new Intent(ctx, AboutActivity.class);
		ctx.startActivity(it);
	}
	
    @Override
    protected int hideMenu() {
    	return R.id.action_sobre;
    }

    @Override
    protected boolean homeAsUpEnabled() {
    	return false;
    }
}