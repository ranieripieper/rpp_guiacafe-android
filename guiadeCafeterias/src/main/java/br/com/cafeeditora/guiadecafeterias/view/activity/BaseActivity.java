package br.com.cafeeditora.guiadecafeterias.view.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import br.com.cafeeditora.guiadecafeterias.R;

import com.doisdoissete.android.util.ddsutil.exception.ConnectionNotFoundException;
import com.doisdoissete.android.util.ddsutil.gps.GPSTracker;
import com.doisdoissete.android.util.ddsutil.service.asynctask.AsyncTaskService;
import com.doisdoissete.android.util.ddsutil.view.custom.TouchBlackHoleView;
import com.doisdoissete.android.util.ddsutil.view.custom.loading.LoadingView;
import com.doisdoissete.android.util.ddsutil.view.custom.util.FontUtilCache;
import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;
import com.nispok.snackbar.listeners.ActionClickListener;

public abstract class BaseActivity extends ActionBarActivity {

	private ProgressDialog progress;
	protected LoadingView loadingView;
	protected TouchBlackHoleView black_hole;
	protected Snackbar mSnackBar;
	protected GPSTracker gps;
	private Snackbar mSnackBarGps;
	private static final int REQUEST_CODE_GPS = 999;

	protected AsyncTaskService async;
	
/*	@Override
	protected void onNewIntent(Intent intent) {
	    super.onNewIntent(intent);
	    if (customOpenClose()) {
			overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
		}
	}*/
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		if (customOpenClose()) {
			overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
		}
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setDisplayUseLogoEnabled(false);
        getSupportActionBar().setLogo(null);
        
        int titleId = getResources().getIdentifier("action_bar_title", "id", "android");
        if (titleId > 0) {
        	TextView titleTextView = (TextView) findViewById(titleId);
            if (titleTextView != null) {
            	FontUtilCache.setCustomFont(BaseActivity.this, titleTextView, "fonts/BrixSlab-Bold.otf");
            }
        }
	}
	
	protected abstract boolean homeAsUpEnabled();
	
	@Override
	public void setContentView(int layoutResID) {
		super.setContentView(layoutResID);
		
		if (homeAsUpEnabled()) {			
			View headerView = getLayoutInflater().inflate(R.layout.include_header_back_button, null);
			getSupportActionBar().setCustomView(headerView);
			getSupportActionBar().setDisplayShowCustomEnabled(true);
			headerView.findViewById(R.id.img_back).setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					finish();
				}
			});
			
			getSupportActionBar().setDisplayHomeAsUpEnabled(false);
			getSupportActionBar().setDisplayShowTitleEnabled(false);
			getSupportActionBar().setDisplayUseLogoEnabled(false);
			
		}		
		
		loadingView = (LoadingView)findViewById(R.id.progress_bar);
		black_hole = (TouchBlackHoleView)findViewById(R.id.black_hole);
		
	}
	
	protected boolean customOpenClose() {
		return true;
	}
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		if (customOpenClose()) {
			overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
		}
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		if (customOpenClose()) {
			overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
		}
	}
	
/*	@Override
	protected void onRestart() {
		super.onRestart();
		if (customOpenClose()) {
			overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
		}
		
	}*/
	
	protected boolean hiddenBackButton() {
		return true;
	}
	
	public void showWaitDialog(boolean blockScreen) {
		if (black_hole != null) {
			if (blockScreen) {
				black_hole.disable_touch(true);
				black_hole.setVisibility(View.VISIBLE);
			} else {
				black_hole.disable_touch(false);
				black_hole.setVisibility(View.GONE);
			}
		}
		
		if (loadingView != null) {
			loadingView.setVisibility(View.VISIBLE);
		} else {
			progress = ProgressDialog.show(this, getString(R.string.title_aguarde),
					getString(R.string.msg_aguarde), true);
			progress.show();
		}
		if (findViewById(R.id.content) != null) {
			findViewById(R.id.content).setVisibility(View.GONE);
		}
	}
	
	public void showWaitDialog() {
		showWaitDialog(true);
	}
	
	public void dismissWaitDialog(boolean showContent) {
		if (black_hole != null) {
			black_hole.disable_touch(false);
			black_hole.setVisibility(View.GONE);
		}
		if (loadingView != null) {
			loadingView.setVisibility(View.GONE);
		}
		if (progress != null) {
			progress.dismiss();
		}
		
		if (showContent && findViewById(R.id.content) != null) {
			findViewById(R.id.content).setVisibility(View.VISIBLE);
		}
	}
	
	public void showError(Exception e) {
		showError(e, false);
		
	}
	
	public void showError(Exception e, boolean simpleSnack) {
		dismissWaitDialog(false);
		if (e != null) {
			int resMsg = R.string.msg_ocorreu_erro;
			if (e instanceof ConnectionNotFoundException) {
				resMsg = R.string.msg_sem_conexao;
			}
			if (simpleSnack) {
				showSimpleSnack(resMsg);
			} else {
				showSnack(resMsg);
			}
	        
		} else {
			if (simpleSnack) {
				showSimpleSnack(R.string.msg_ocorreu_erro);
			} else {
				showSnack(R.string.msg_ocorreu_erro);
			}
		}
		
	}
	
    protected void showSnack(int msg) {
    	ActionClickListener actionClickListener = new ActionClickListener() {
			
			@Override
			public void onActionClicked(Snackbar snackbar) {
				refresh();
			}
		};
    	
    	mSnackBar = Snackbar.with(BaseActivity.this)
                .text(msg)
                .actionLabel(R.string.txt_tentar_novemante)
                .actionListener(actionClickListener)
                .duration(Snackbar.SnackbarDuration.LENGTH_INDEFINITE);
    	 SnackbarManager.show(mSnackBar);
    }
    
    protected void showSimpleSnack(int msg) {
    	showSimpleSnack(msg, Snackbar.SnackbarDuration.LENGTH_SHORT);
    }
    
    protected void showSimpleSnack(int msg, Snackbar.SnackbarDuration duration) {
    	mSnackBar = Snackbar.with(BaseActivity.this)
                .text(msg)
                .duration(duration);
    	 SnackbarManager.show(mSnackBar);
    }
      
    protected void refresh() { 
    	if (mSnackBar != null) {
    		mSnackBar.dismiss();
    	} 
    	SnackbarManager.dismiss();
    }
    
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString("WORKAROUND_FOR_BUG_19917_KEY", "WORKAROUND_FOR_BUG_19917_VALUE");
        super.onSaveInstanceState(outState);
    }
    
    protected boolean verifyGpsEnable() {
		if (gps == null) {
			gps = new GPSTracker(BaseActivity.this);
		}
		if(gps.canGetLocation()){
			return true;
		}
		
		showSnackHabilitarGps();
		
		return false;
	}
    
    protected void showSnackHabilitarGps() {
    	if (mSnackBarGps == null) {
	    	ActionClickListener actionClickListener = new ActionClickListener() {
				
				@Override
				public void onActionClicked(Snackbar snackbar) {
					Intent callGPSSettingIntent = new Intent(
	                        android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
	                startActivityForResult(callGPSSettingIntent, REQUEST_CODE_GPS);
				}
			};
	    	
			mSnackBarGps = Snackbar.with(BaseActivity.this)
	                .text(R.string.msg_habilitar_gps)
	                .actionLabel(R.string.txt_habilitar_gps)
	                .actionListener(actionClickListener)
	                .duration(Snackbar.SnackbarDuration.LENGTH_INDEFINITE);
		}
		
		SnackbarManager.show(mSnackBarGps);
	}
    
	protected void onActivityResult(int requestCode, int resultCode, Intent data){
		final LocationManager mlocManager = (LocationManager) BaseActivity.this.getSystemService(Context.LOCATION_SERVICE);
        if(requestCode == REQUEST_CODE_GPS && resultCode == 0){
            String provider = Settings.Secure.getString(getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            if(provider != null){
            	
            	Location loc = gps.getLocation();

        		if (mSnackBarGps != null) {
        			mSnackBarGps.dismiss();
        			SnackbarManager.dismiss();
        		}
        		
            	if (loc == null) {
            		final LocationListener locationListener = new LocationListener() {
                    	Boolean search = false;
                        public void onLocationChanged(Location location) {
                        	synchronized (search) {
                        		if (!search) {
                        			search = true;
    	                    		mlocManager.removeUpdates(this);
    	                    		gpsEnable(location);  
                        		}
    						}              	
                        }

                        public void onStatusChanged(String provider, int status, Bundle extras) {}

                        public void onProviderEnabled(String provider) {}

                        public void onProviderDisabled(String provider) {}
                      };

                      mlocManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1, 0, locationListener);
                      mlocManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 2, 0, locationListener);
                      showWaitDialog();
            	} else {
            		gpsEnable(loc);
            	}
            } else {
            	showSnackHabilitarGps();
            }
        }
    }
	
	protected void gpsEnable(Location location) {
		
	}
	
   @Override
   public boolean onCreateOptionsMenu(Menu menu) {
		if (getResourceMenu() != -1) {
			getMenuInflater().inflate(getResourceMenu(), menu);
		}

		if (hideMenu() != -1) {
			menu.findItem(hideMenu()).setVisible(false);
		}
		return super.onCreateOptionsMenu(menu);
   }
   
   protected int getResourceMenu() {
	   return R.menu.internas;   
   }
   
   @Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == android.R.id.home) {
			finish();
		} else if (id == R.id.action_cafeterias) {
			MainActivity.showActivity(BaseActivity.this);
			if (finishActivity()) {
				finish();
			}
			return true;
		} else if (id == R.id.action_top_20) {
			ListCafeteriasActivity.showTop20Activity(BaseActivity.this);
			if (finishActivity()) {
				finish();
			}
			
			return true;
		} else if (id == R.id.action_cafeterias_revelacao) {
			ListCafeteriasActivity.showRevelacaoActivity(BaseActivity.this);
			if (finishActivity()) {
				finish();
			}
			
			return true;
		} else if (id == R.id.action_favoritos) {
			ListCafeteriasActivity.showFavoritosActivity(BaseActivity.this);
			if (finishActivity()) {
				finish();
			}
			
			return true;
		} else if (id == R.id.action_settings) {
			SettingsActivity.showActivity(BaseActivity.this);
			if (finishActivity()) {
				finish();
			}
			
			return true;
		} else if (id == R.id.action_sobre) {
			AboutActivity.showActivity(BaseActivity.this);
			if (finishActivity()) {
				finish();
			}
			
			return true;
		} else if (id == R.id.action_busca_de_cafeteria) {
			SearchActivity.showActivity(BaseActivity.this);
			if (finishActivity()) {
				finish();
			}
			
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
   
	protected abstract int hideMenu();

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (async != null) {
			async.cancel(true);
		}
	}
	
	
	
	private boolean finishActivity() {
		return false;
	}
}
