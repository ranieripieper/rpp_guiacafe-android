package br.com.cafeeditora.guiadecafeterias.view.activity;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpMethod;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import br.com.cafeeditora.guiadecafeterias.GuiaCafeteriasApplication;
import br.com.cafeeditora.guiadecafeterias.R;
import br.com.cafeeditora.guiadecafeterias.model.Cafeteria;
import br.com.cafeeditora.guiadecafeterias.model.ItemsHouse;
import br.com.cafeeditora.guiadecafeterias.model.Photo;
import br.com.cafeeditora.guiadecafeterias.model.ResultAvaliar;
import br.com.cafeeditora.guiadecafeterias.model.ResultCafeteria;
import br.com.cafeeditora.guiadecafeterias.model.ResultFavoritar;
import br.com.cafeeditora.guiadecafeterias.model.StringInteger;
import br.com.cafeeditora.guiadecafeterias.model.UserData;
import br.com.cafeeditora.guiadecafeterias.util.ColorStateUtil;
import br.com.cafeeditora.guiadecafeterias.util.Constants;
import br.com.cafeeditora.guiadecafeterias.util.Preferences;
import br.com.cafeeditora.guiadecafeterias.util.RateUtil;
import br.com.cafeeditora.guiadecafeterias.view.activity.adapter.ImageAdapter;
import br.com.cafeeditora.guiadecafeterias.view.custom.DescriptionAnimation;

import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.Indicators.PagerIndicator.IndicatorVisibility;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.doisdoissete.android.util.ddsutil.Util;
import com.doisdoissete.android.util.ddsutil.gps.GpsUtil;
import com.doisdoissete.android.util.ddsutil.service.asynctask.ObserverAsyncTask;
import com.doisdoissete.android.util.ddsutil.service.asynctask.ServiceBuilder;
import com.doisdoissete.android.util.ddsutil.view.custom.listview.ExpandableHeightGridView;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

public class DetailStoreActivity extends BaseActivity implements BaseSliderView.OnSliderClickListener {

	public static final String PARAM_INTENT_CAFETERIA = "PARAM_INTENT_CAFETERIA";

	private Cafeteria cafeteria;
	
	//componentes de tela
	private TextView txtEstado;
	private ImageView imgPin;
	private TextView txtDistancia;
	private TextView txtDesde;
	private TextView txtNomeCaf;
	private ImageView imgEsperessoIndica;
	private ImageView imgRevelacao;
	private ImageView imgTop20;
	private ImageView imgRate1;
	private ImageView imgRate2;
	private ImageView imgRate3;
	private ImageView imgRate4;
	private ImageView imgRate5;
	private SliderLayout mSlider;
	
	private TextView txtTitleServe;
	private TextView txtTextoServe;
	private TextView txtTitleExtraido;
	private TextView txtTextoExtraido;
	private TextView txtTitleBeber;
	private TextView txtTextoBeber;
	private TextView txtTitleComer;
	private TextView txtTextoComer;
	private TextView txtTitlePontoForte;
	private TextView txtTextoPontoForte;
	private TextView txtTitleServico;
	private TextView txtTextoServicoAddr;
	private TextView txtTextoServicoHours;
	private TextView txtTextoServicoPhone;
	private TextView txtTextoServicoPlaces;
	private TextView txtTextoServicoSite;
	private TextView txtTextoServicoCartoes;
	private TextView txtTextoServicoTickets;
	private TextView txtTitleResenha;
	private TextView txtTextoResenha;
	private TextView txtNrAvaliacoes;
	
	//avaliar
	private View viewAvaliar;
	private ImageView imgAvaliarRate1;
	private ImageView imgAvaliarRate2;
	private ImageView imgAvaliarRate3;
	private ImageView imgAvaliarRate4;
	private ImageView imgAvaliarRate5;
	private AlertDialog dialogAvaliar;
	
	private AlertDialog dialogServicos1;
	private AlertDialog dialogServicos2;
	
	//logar
	private View viewLogar;
	private AlertDialog dialogLogar;
	
	private ExpandableHeightGridView gridView;
	private ExpandableHeightGridView gridViewServicos;
	private int rating = 0;
	
	private void initComponentes() {
		txtEstado = (TextView)findViewById(R.id.txt_estado);
		imgPin = (ImageView)findViewById(R.id.img_pin);
		txtDistancia = (TextView)findViewById(R.id.txt_distancia);
		txtDesde = (TextView)findViewById(R.id.txt_desde);
		txtNomeCaf = (TextView)findViewById(R.id.txt_nome_cafeteria);
		imgEsperessoIndica = (ImageView)findViewById(R.id.img_esperesso_indica);
		imgRevelacao = (ImageView)findViewById(R.id.img_revelacao);
		imgTop20 = (ImageView)findViewById(R.id.img_top_20);
		
		imgRate1 = (ImageView)findViewById(R.id.img_rate_1);
		imgRate2 = (ImageView)findViewById(R.id.img_rate_2);
		imgRate3 = (ImageView)findViewById(R.id.img_rate_3);
		imgRate4 = (ImageView)findViewById(R.id.img_rate_4);
		imgRate5 = (ImageView)findViewById(R.id.img_rate_5);

		mSlider = (SliderLayout)findViewById(R.id.img_slider);
		
		txtTitleServe = (TextView)findViewById(R.id.txt_title_serve);
		txtTextoServe = (TextView)findViewById(R.id.txt_texto_serve);
		txtTitleExtraido = (TextView)findViewById(R.id.txt_title_extraido);
		txtTextoExtraido = (TextView)findViewById(R.id.txt_texto_extraido);
		txtTitleBeber = (TextView)findViewById(R.id.txt_title_para_beber);
		txtTextoBeber = (TextView)findViewById(R.id.txt_texto_para_beber);
		txtTitleComer = (TextView)findViewById(R.id.txt_title_para_comer);
		txtTextoComer = (TextView)findViewById(R.id.txt_texto_para_comer);
		txtTitlePontoForte = (TextView)findViewById(R.id.txt_title_ponto_forte);
		txtTextoPontoForte = (TextView)findViewById(R.id.txt_texto_ponto_forte);
		txtTitleResenha = (TextView)findViewById(R.id.txt_title_resenha);
		txtTextoResenha = (TextView)findViewById(R.id.txt_texto_resenha);
		txtTitleServico = (TextView)findViewById(R.id.txt_title_servico);
		txtTextoServicoAddr = (TextView)findViewById(R.id.txt_texto_servico_address);
		txtTextoServicoHours = (TextView)findViewById(R.id.txt_texto_servico_hours);
		txtTextoServicoPhone = (TextView)findViewById(R.id.txt_texto_servico_phone);
		txtTextoServicoPlaces = (TextView)findViewById(R.id.txt_texto_servico_places);
		txtTextoServicoCartoes = (TextView)findViewById(R.id.txt_texto_servico_cartoes);
		txtTextoServicoTickets = (TextView)findViewById(R.id.txt_texto_servico_ticket);
		txtTextoServicoSite = (TextView)findViewById(R.id.txt_texto_servico_site);
		txtNrAvaliacoes = (TextView)findViewById(R.id.txt_nr_avaliacoes);
		
		gridView = (ExpandableHeightGridView) findViewById(R.id.grid_view_selos);
		gridViewServicos = (ExpandableHeightGridView) findViewById(R.id.grid_view_selos_servicos);
		
		gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				showServicos1();
			}
		});
		
		gridViewServicos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				showServicos2();
			}
		});
		
		findViewById(R.id.bt_avaliar).setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				showAvaliarCafeteria();
			}
		});
		viewAvaliar = getLayoutInflater().inflate(R.layout.dialog_avaliar, null);
		    
	    imgAvaliarRate1 = (ImageView)viewAvaliar.findViewById(R.id.img_rate_1);
	    imgAvaliarRate2 = (ImageView)viewAvaliar.findViewById(R.id.img_rate_2);
	    imgAvaliarRate3 = (ImageView)viewAvaliar.findViewById(R.id.img_rate_3);
	    imgAvaliarRate4 = (ImageView)viewAvaliar.findViewById(R.id.img_rate_4);
	    imgAvaliarRate5 = (ImageView)viewAvaliar.findViewById(R.id.img_rate_5);
		    
	}
	
	private void showServicos1() {
		dialogServicos1.show();
	}
	
	private void showServicos2() {
		dialogServicos2.show();
	}
	
   @Override
   public boolean onCreateOptionsMenu(Menu menu) {
		boolean result = super.onCreateOptionsMenu(menu);
		MenuItem menuItem = menu.findItem(R.id.action_favoritar);
		if (cafeteria != null && cafeteria.getUserData() != null && cafeteria.getUserData().isFavorite() ) {
			menuItem.setIcon(R.drawable.icn_filled_fav);
		} else {
			menuItem.setIcon(R.drawable.icn_fav);
		}
        
		return result;
   }
	   
   
   private void shareCafeteria() {
	   Intent sendIntent = new Intent();
	   sendIntent.setAction(Intent.ACTION_SEND);
	   sendIntent.putExtra(Intent.EXTRA_SUBJECT, cafeteria.getName());
	   sendIntent.putExtra(Intent.EXTRA_TITLE, cafeteria.getName());
	   sendIntent.putExtra(Intent.EXTRA_TEXT, getString(R.string.txt_share, cafeteria.getName(), Constants.URL_SHARE));
	   sendIntent.setType("text/plain");
	   startActivity(Intent.createChooser(sendIntent, getResources().getText(R.string.txt_compartilhar_cafeteria)));
   }
   
   private AlertDialog createServicos(List<StringInteger> values) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);

		View view = getLayoutInflater().inflate(R.layout.dialog_show_servicos,
				null);

		builder.setView(view);

		final AlertDialog res = builder.create();

		view.findViewById(R.id.img_close).setOnClickListener(
				new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						res.dismiss();
					}
				});

		ListView lstView = (ListView) view.findViewById(R.id.list_view);
		lstView.setAdapter(new ImageAdapter(this, values,
				R.layout.include_text_image_view));

		return res;
	}
	
	
	
	private void showAvaliarCafeteria() {
		if (userLogado(R.string.txt_avaliar_nao_loagado)) {
			
			if (dialogAvaliar == null) {
				AlertDialog.Builder builder = new AlertDialog.Builder(this);
	
			    builder.setView(viewAvaliar);
		  
			    imgAvaliarRate1.setOnClickListener(rateClick);
			    imgAvaliarRate2.setOnClickListener(rateClick);
			    imgAvaliarRate3.setOnClickListener(rateClick);
			    imgAvaliarRate4.setOnClickListener(rateClick);
			    imgAvaliarRate5.setOnClickListener(rateClick);
			    dialogAvaliar = builder.create();
			    
			    viewAvaliar.findViewById(R.id.bt_ok).setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						if (rating <= 0) {
							showSimpleSnack(R.string.txt_msg_de_uma_nota);
						} else {
							avaliarCafeteria(rating);
							dialogAvaliar.dismiss();
						}
					}
				});
			    
			    viewAvaliar.findViewById(R.id.img_close).setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						dialogAvaliar.dismiss();
					}
				});
			}
			
		    
			if (cafeteria.getUserData() != null) {
				RateUtil.setRate(new ImageView[] {imgAvaliarRate1, imgAvaliarRate2, imgAvaliarRate3, imgAvaliarRate4, imgAvaliarRate5}, 
						cafeteria.getUserData().getRating(), 
						new int[]{R.drawable.icn_rate_detail, R.drawable.icn_rate_almost_filled_detail, R.drawable.icn_rate_filled_detail});
				
			}
			
			dialogAvaliar.show();
		}
	}
	
	private View.OnClickListener rateClick = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			
			if (v.equals(imgAvaliarRate1)) {
				rating = 1;
				imgAvaliarRate1.setImageResource(R.drawable.icn_rate_filled_detail);
				imgAvaliarRate2.setImageResource(R.drawable.icn_rate_detail);
				imgAvaliarRate3.setImageResource(R.drawable.icn_rate_detail);
				imgAvaliarRate4.setImageResource(R.drawable.icn_rate_detail);
				imgAvaliarRate5.setImageResource(R.drawable.icn_rate_detail);
			} else if (v.equals(imgAvaliarRate2)) {
				rating = 2;
				imgAvaliarRate1.setImageResource(R.drawable.icn_rate_filled_detail);
				imgAvaliarRate2.setImageResource(R.drawable.icn_rate_filled_detail);
				imgAvaliarRate3.setImageResource(R.drawable.icn_rate_detail);
				imgAvaliarRate4.setImageResource(R.drawable.icn_rate_detail);
				imgAvaliarRate5.setImageResource(R.drawable.icn_rate_detail);
			} else if (v.equals(imgAvaliarRate3)) {
				rating = 3;
				imgAvaliarRate1.setImageResource(R.drawable.icn_rate_filled_detail);
				imgAvaliarRate2.setImageResource(R.drawable.icn_rate_filled_detail);
				imgAvaliarRate3.setImageResource(R.drawable.icn_rate_filled_detail);
				imgAvaliarRate4.setImageResource(R.drawable.icn_rate_detail);
				imgAvaliarRate5.setImageResource(R.drawable.icn_rate_detail);
			} else if (v.equals(imgAvaliarRate4)) {
				rating = 4;
				imgAvaliarRate1.setImageResource(R.drawable.icn_rate_filled_detail);
				imgAvaliarRate2.setImageResource(R.drawable.icn_rate_filled_detail);
				imgAvaliarRate3.setImageResource(R.drawable.icn_rate_filled_detail);
				imgAvaliarRate4.setImageResource(R.drawable.icn_rate_filled_detail);
				imgAvaliarRate5.setImageResource(R.drawable.icn_rate_detail);
			} else if (v.equals(imgAvaliarRate5)) {
				rating = 5;
				imgAvaliarRate1.setImageResource(R.drawable.icn_rate_filled_detail);
				imgAvaliarRate2.setImageResource(R.drawable.icn_rate_filled_detail);
				imgAvaliarRate3.setImageResource(R.drawable.icn_rate_filled_detail);
				imgAvaliarRate4.setImageResource(R.drawable.icn_rate_filled_detail);
				imgAvaliarRate5.setImageResource(R.drawable.icn_rate_filled_detail);
			}
		}
	};
	
	private void avaliarCafeteria(final int rating) {
		//verifica se o usuário está logado
		if (!userLogado(R.string.txt_avaliar_nao_loagado)) {
			return;
		}
		
		ObserverAsyncTask<ResultAvaliar> observer = new ObserverAsyncTask<ResultAvaliar>() {
			@Override
			public void onPreExecute() {
				showWaitDialog();
			}

			@Override
			public void onPostExecute(ResultAvaliar result) {
				if (result != null && result.isSuccess()) {
					dismissWaitDialog(true);
					cafeteria.getRatings().setAverage(rating);
					callServiceCafeteria(R.string.msg_voto_registrado);
				} else {
					showError(null);
				}
			}

			@Override
			public void onCancelled() {
				dismissWaitDialog(false);
			}

			@Override
			public void onError(Exception e) {
				showError(e);
			}
		};

		MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
		params.add(Cafeteria.PARAM_ACCESS_TOKEN, Preferences.getToken(DetailStoreActivity.this));
		params.add(Cafeteria.PARAM_RATING, String.valueOf(rating));
		
		String url = Constants.AVALIAR;
		url = url.replace(Cafeteria.PARAM_ID, cafeteria.getUuid());
		
		ServiceBuilder sb = new ServiceBuilder();
		sb.setUrl(url)
			.setObserverAsyncTask(observer)
			.setNeedConnection(true)
			.setHttpMethod(HttpMethod.POST)
			.setParams(params);
	
		async = sb.mappingInto(DetailStoreActivity.this, ResultAvaliar.class);
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			async.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			async.execute();
		}
				
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.detail_store_activity);

		Bundle data = getIntent().getExtras();
		cafeteria = (Cafeteria) data.getParcelable(PARAM_INTENT_CAFETERIA);
		
		initComponentes();

        populateView();
	}
	
	private void populateView() {
		populateView(true);
	}
	
	private void populateView(boolean initSlider) {
		if (cafeteria != null) {
			String state = getString(cafeteria.getAddress().getStateToCompare());
			int color = ColorStateUtil.getColorByState(state, DetailStoreActivity.this);
			txtEstado.setText(state);
			txtEstado.setBackgroundColor(color);
			
			String dist = GpsUtil.distance(DetailStoreActivity.this, cafeteria.getAddress().getGeolocation().getLatitude(), cafeteria.getAddress().getGeolocation().getLongitude());
			if (!TextUtils.isEmpty(dist)) {
				txtDistancia.setText(dist);
				txtDistancia.setTextColor(color);
				imgPin.setImageResource(ColorStateUtil.getPinByState(state));
				
				txtDistancia.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						Util.navigateTo(DetailStoreActivity.this, cafeteria.getAddress().getGeolocation().getLongitude(), cafeteria.getAddress().getGeolocation().getLatitude(), cafeteria.getName());
					}
				});
				
				imgPin.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						Util.navigateTo(DetailStoreActivity.this, cafeteria.getAddress().getGeolocation().getLongitude(), cafeteria.getAddress().getGeolocation().getLatitude(), cafeteria.getName());
					}
				});
			} else {
				txtDistancia.setVisibility(View.GONE);
				imgPin.setVisibility(View.GONE);
			}
			
			txtNomeCaf.setText(cafeteria.getName());
			
			if (cafeteria.isCafeteriaRevelacao()) {
				imgRevelacao.setVisibility(View.VISIBLE);
			} else {
				imgRevelacao.setVisibility(View.GONE);
			}
			
			if (cafeteria.isFeatured()) {
				imgEsperessoIndica.setVisibility(View.VISIBLE);
			} else {
				imgEsperessoIndica.setVisibility(View.GONE);
			}
			
			if (cafeteria.isTop20()) {
				imgTop20.setVisibility(View.VISIBLE);
			} else {
				imgTop20.setVisibility(View.GONE);
			}
			
			if (cafeteria.getRatings() != null) {
				
				if (cafeteria.getRatings().getTotal() != 1) {
					txtNrAvaliacoes.setText(getString(R.string.txt_nr_avaliacoes, cafeteria.getRatings().getTotal()));	
				} else {
					txtNrAvaliacoes.setText(getString(R.string.txt_nr_avaliacao, cafeteria.getRatings().getTotal()));
				}
				
				RateUtil.setRate(new ImageView[] {imgRate1, imgRate2, imgRate3, imgRate4, imgRate5}, 
						cafeteria.getRatings().getAverage(), 
						new int[]{R.drawable.icn_rate_detail, R.drawable.icn_rate_almost_filled_detail, R.drawable.icn_rate_filled_detail});
			}

			if (initSlider) {
				initSlider(cafeteria.getPhotos());
			}
			
			//textos
			if (cafeteria.getInformations() != null) {
				if (!TextUtils.isEmpty(cafeteria.getInformations().getServe())) {
					txtTitleServe.setTextColor(color);
					txtTextoServe.setText(cafeteria.getInformations().getServe());
				} else {
					txtTitleServe.setVisibility(View.GONE);
					txtTextoServe.setVisibility(View.GONE);
				}
				
				if (!TextUtils.isEmpty(cafeteria.getInformations().getExtraction())) {
					txtTitleExtraido.setTextColor(color);
					txtTextoExtraido.setText(cafeteria.getInformations().getExtraction());
				} else {
					txtTitleExtraido.setVisibility(View.GONE);
					txtTextoExtraido.setVisibility(View.GONE);
				}
				
				if (!TextUtils.isEmpty(cafeteria.getInformations().getToDrink())) {
					txtTitleBeber.setTextColor(color);
					txtTextoBeber.setText(cafeteria.getInformations().getToDrink());
				} else {
					txtTitleBeber.setVisibility(View.GONE);
					txtTextoBeber.setVisibility(View.GONE);
				}
				
				if (!TextUtils.isEmpty(cafeteria.getInformations().getToEat())) {
					txtTitleComer.setTextColor(color);
					txtTextoComer.setText(cafeteria.getInformations().getToEat());
				} else {
					txtTitleComer.setVisibility(View.GONE);
					txtTextoComer.setVisibility(View.GONE);
				}
				
				if (!TextUtils.isEmpty(cafeteria.getInformations().getStrongPoints())) {
					txtTitlePontoForte.setTextColor(color);
					txtTextoPontoForte.setText(cafeteria.getInformations().getStrongPoints());
				} else {
					txtTitlePontoForte.setVisibility(View.GONE);
					txtTextoPontoForte.setVisibility(View.GONE);
				}
			} else {
				txtTitleServe.setVisibility(View.GONE);
				txtTextoServe.setVisibility(View.GONE);
				txtTitleExtraido.setVisibility(View.GONE);
				txtTextoExtraido.setVisibility(View.GONE);
				txtTitleBeber.setVisibility(View.GONE);
				txtTextoBeber.setVisibility(View.GONE);
				txtTitleComer.setVisibility(View.GONE);
				txtTextoComer.setVisibility(View.GONE);
				txtTitlePontoForte.setVisibility(View.GONE);
				txtTextoPontoForte.setVisibility(View.GONE);
			}
			
			if (!TextUtils.isEmpty(cafeteria.getResume())) {
				txtTitleResenha.setTextColor(color);
				txtTextoResenha.setText(cafeteria.getResume());
			} else {
				txtTitleResenha.setVisibility(View.GONE);
				txtTextoResenha.setVisibility(View.GONE);
			}
			
			txtTitleServico.setTextColor(color);
			txtTextoServicoAddr.setText(cafeteria.getFormatedAddress());
			if (cafeteria.getAddress() != null && cafeteria.getAddress().getGeolocation() != null
					&& cafeteria.getAddress().getGeolocation().getLongitude() != null
					&& cafeteria.getAddress().getGeolocation().getLatitude() != null) {
				txtTextoServicoAddr.setOnClickListener(new View.OnClickListener() {
					
					@Override
					public void onClick(View v) {
						Util.navigateTo(DetailStoreActivity.this, cafeteria.getAddress().getGeolocation().getLongitude(), 
								cafeteria.getAddress().getGeolocation().getLatitude(), 
								cafeteria.getName());
					}
				});
			}
			
			txtTextoServicoHours.setText(cafeteria.getFormatedHours());
			if (cafeteria.getContact() != null) {
				if (!TextUtils.isEmpty(cafeteria.getContact().getPhone())) {
					txtTextoServicoPhone.setText(getString(R.string.txt_telefone, cafeteria.getContact().getPhone()));
					txtTextoServicoPhone.setOnClickListener(new View.OnClickListener() {
						
						@Override
						public void onClick(View v) {
							Util.callTo(DetailStoreActivity.this, cafeteria.getContact().getPhone());
						}
					});
				} else {
					txtTextoServicoPhone.setVisibility(View.GONE);
				}
				if (!TextUtils.isEmpty(cafeteria.getContact().getWebsite())) {
					txtTextoServicoSite.setText(cafeteria.getContact().getWebsite()); 
					txtTextoServicoSite.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							Util.openBrowser(DetailStoreActivity.this, cafeteria.getContact().getWebsite());
						}
					});
				} else {
					txtTextoServicoSite.setVisibility(View.GONE);
				}
			} else {
				txtTextoServicoPhone.setVisibility(View.GONE);
				txtTextoServicoSite.setVisibility(View.GONE);
			}
						
			if (cafeteria.getEstablishment() != null && !TextUtils.isEmpty(cafeteria.getEstablishment().getCapacityHome())) {
				txtTextoServicoPlaces.setText(getString(R.string.txt_nr_lugares, cafeteria.getEstablishment().getCapacityHome()));
			} else {
				txtTextoServicoPlaces.setVisibility(View.GONE);
			}	
			
			if (cafeteria.getEstablishment() != null && !TextUtils.isEmpty(cafeteria.getEstablishment().getOpening())) {
				txtDesde.setText(cafeteria.getEstablishment().getOpening());
			} else {
				txtDesde.setVisibility(View.GONE);
			}

			if (!TextUtils.isEmpty(cafeteria.getFormatedPaymentsBrands())) {
				txtTextoServicoCartoes.setText(cafeteria.getFormatedPaymentsBrands());
			} else {
				findViewById(R.id.layout_cartoes).setVisibility(View.GONE);
			}
			
			if (!TextUtils.isEmpty(cafeteria.getFormatedPaymentsTypeTickets())) {
				txtTextoServicoTickets.setText(cafeteria.getFormatedPaymentsTypeTickets());
			} else {
				findViewById(R.id.layout_tickets).setVisibility(View.GONE);
			}

			populateGridView();
			
	        populateGridViewServicos();
	        
	        //workaround por conta do gridview
	        final ScrollView scroll = (ScrollView)findViewById(R.id.scroll_view);
	        scroll.postDelayed(new Runnable() {
				@Override
				public void run() {
					scroll.scrollTo(0, 0);
				}
			}, 1);
	        
	        invalidateOptionsMenu();
		}		
	}
	
	private void populateGridView() {
        List<StringInteger> lst = new ArrayList<StringInteger>();
        
        if (cafeteria.getCoffee() != null) {
        	if (cafeteria.getCoffee().getCoffeeAlternateTypes() != null 
        			&& cafeteria.getCoffee().getCoffeeAlternateTypes().isDecaffeinated()) {
        		lst.add(new StringInteger(10, getString(R.string.txt_oferece_cafe_descafeinado), R.drawable.icn_decafeinado));
        	}
        	if (cafeteria.getCoffee().isTnt()) {
        		lst.add(new StringInteger(5, getString(R.string.txt_campeonato_tnt), R.drawable.icn_tnt));
        	}
        	if (cafeteria.getCoffee().isRoasting()) {
        		lst.add(new StringInteger(4, getString(R.string.txt_torra_cafe), R.drawable.icn_torra_cafe));
        	}
        	if (cafeteria.getCoffee().isTasting()) {
        		lst.add(new StringInteger(3, getString(R.string.txt_oferece_degustacao), R.drawable.icn_degustacao));
        	}
        	if (cafeteria.getCoffee().hasAlternateTypes()) {
        		lst.add(new StringInteger(11, getString(R.string.txt_restricoes_alimentares), R.drawable.icn_restricoes));
        	}
        	if (cafeteria.getCoffee().hasAwards()) {
        		lst.add(new StringInteger(13, getString(R.string.txt_tem_premios), R.drawable.icn_premiado));
        	}
        	if (cafeteria.getCoffee().isToGoBuy()) {
        		lst.add(new StringInteger(14, getString(R.string.txt_sair_tomando), R.drawable.icn_para_levar));
        	}
        	if (cafeteria.getCoffee().isToBuy()) {
        		lst.add(new StringInteger(15, getString(R.string.txt_cafe_graos_moido), R.drawable.icn_presente));
        	}
        }

        if (cafeteria.getEstablishment() != null) {
        	if (cafeteria.getEstablishment().getItemsHouse() != null) {
        		ItemsHouse items = cafeteria.getEstablishment().getItemsHouse();
            	if (items.isWifi()) {
            		lst.add(new StringInteger(1, getString(R.string.txt_tem_wifi), R.drawable.icn_wifi));
            	}
            	if (items.isOpenAir()) {
            		lst.add(new StringInteger(9, getString(R.string.txt_tem_area_ar_livre), R.drawable.icn_area_ar_livre));
            	}
            	if (items.isAttested()) {
            		lst.add(new StringInteger(2, getString(R.string.txt_tem_barista), R.drawable.icn_certificado));
            	}
        	}
        	
        	if (cafeteria.getEstablishment().isLiveMusic()) {
        		lst.add(new StringInteger(8, getString(R.string.txt_tem_musica), R.drawable.icn_musica));
        	}
        	
        	if (cafeteria.getEstablishment().isCourses()) {
        		lst.add(new StringInteger(12, getString(R.string.txt_oferece_cursos), R.drawable.icn_cursos));
        	}
        	
        	if (cafeteria.getEstablishment().isCloseMetroPoint()) {
        		lst.add(new StringInteger(7, getString(R.string.txt_prox_estacao_metro), R.drawable.icn_trem));
        	}
        	
        	if (cafeteria.getEstablishment().isCloseTouristPoint()) {
        		lst.add(new StringInteger(6, getString(R.string.txt_prox_ponto_turistico), R.drawable.icn_turistico));
        	}
        }
        
        if (lst.size() > 0) {
        	gridView.setExpanded(true);
        	gridView.setAdapter(new ImageAdapter(this, lst));
        }
        
        dialogServicos1 = createServicos(lst);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_favoritar) {
			callFavoritarDesfavoritarService();
		} else if(id == R.id.action_share) {
			shareCafeteria();
		}
		return super.onOptionsItemSelected(item);
	}
	
	private boolean userLogado(int res) {
		if (!Preferences.userIsLogado(DetailStoreActivity.this)) {
			showDialogUsuarioNaoLoadado(res);
			return false;
		}
		return true;
	}
	
	private void showDialogUsuarioNaoLoadado(int resTitle) {
		if (dialogLogar == null) {
			AlertDialog.Builder builder = new AlertDialog.Builder(this);

			viewLogar = getLayoutInflater().inflate(R.layout.dialog_logar, null);
			
		    builder.setView(viewLogar);
	  
		    
		    dialogLogar = builder.create();
		    
		    viewLogar.findViewById(R.id.bt_logar).setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					SettingsActivity.showActivity(DetailStoreActivity.this);
					overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
					dialogLogar.dismiss();
				}
			});
		    
		    viewLogar.findViewById(R.id.img_close).setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					dialogLogar.dismiss();
				}
			});
		}

		((TextView)viewLogar.findViewById(R.id.txt_title)).setText(resTitle);
	    dialogLogar.show();
	}
	
	private void callFavoritarDesfavoritarService() {
		//verifica se o usuário está logado
		if (!userLogado(R.string.txt_favaoritar_nao_loagado)) {
			return;
		}
		
		Preferences.setFavDesfavClicked(DetailStoreActivity.this, true);
		ObserverAsyncTask<ResultFavoritar> observer = new ObserverAsyncTask<ResultFavoritar>() {
			@Override
			public void onPreExecute() {
				showWaitDialog();
			}

			@Override
			public void onPostExecute(ResultFavoritar result) {
				if (result != null && result.isSuccess()) {
					dismissWaitDialog(true);
					if (cafeteria.getUserData() == null) {
						cafeteria.setUserData(new UserData());
					}
					cafeteria.getUserData().setFavorite(!cafeteria.getUserData().isFavorite());
					invalidateOptionsMenu();
					callServiceCafeteria(-1);
				} else {
					showError(null);
				}
			}

			@Override
			public void onCancelled() {
				dismissWaitDialog(false);
			}

			@Override
			public void onError(Exception e) {
				showError(e);
			}
		};

		MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
		params.add(Cafeteria.PARAM_ACCESS_TOKEN, Preferences.getToken(DetailStoreActivity.this));
		
		String url = Constants.FAVORITAR;
		if (cafeteria.getUserData() != null && cafeteria.getUserData().isFavorite()) {
			url = Constants.DESFAVORITAR;
		}
		url = url.replace(Cafeteria.PARAM_ID, cafeteria.getUuid());
		
		ServiceBuilder sb = new ServiceBuilder();
		sb.setUrl(url)
			.setObserverAsyncTask(observer)
			.setNeedConnection(true)
			.setHttpMethod(HttpMethod.POST)
			.setParams(params);
	
		async = sb.mappingInto(DetailStoreActivity.this, ResultFavoritar.class);
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			async.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			async.execute();
		}
	}
	
	
	private void callServiceCafeteria(final int resMsg) {
		
		ObserverAsyncTask<ResultCafeteria> observer = new ObserverAsyncTask<ResultCafeteria>() {
			@Override
			public void onPreExecute() {
				showWaitDialog();
			}

			@Override
			public void onPostExecute(ResultCafeteria result) {
				if (result != null && result.isSuccess() && result.getCafeterias() != null &&
						result.getCafeterias().length > 0) {
					dismissWaitDialog(true);
					cafeteria = result.getCafeterias()[0];
					populateView(false);
					invalidateOptionsMenu();
					if (resMsg != -1) {
						showSimpleSnack(resMsg);
					}
					
				}
			}

			@Override
			public void onCancelled() {
				dismissWaitDialog(false);
			}

			@Override
			public void onError(Exception e) {
				dismissWaitDialog(false);
			}
		};

		MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
		params.add(Cafeteria.PARAM_ACCESS_TOKEN, Preferences.getToken(DetailStoreActivity.this));
		
		String url = Constants.CAFETERIA;

		url = url.replace(Cafeteria.PARAM_ID, cafeteria.getUuid());
		
		ServiceBuilder sb = new ServiceBuilder();
		sb.setUrl(url)
			.setObserverAsyncTask(observer)
			.setNeedConnection(true)
			.setHttpMethod(HttpMethod.GET)
			.setParams(params);
	
		async = sb.mappingInto(DetailStoreActivity.this, ResultCafeteria.class);
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			async.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			async.execute();
		}
	}
	
	private void populateGridViewServicos() {
		List<StringInteger> lst = new ArrayList<StringInteger>();
        
        
        if (cafeteria.getPayments() != null && cafeteria.getPayments().isCheck()) {
        	lst.add(new StringInteger(16, getString(R.string.txt_aceita_cheque), R.drawable.icn_cheque));
        }

        if (cafeteria.getEstablishment() != null && cafeteria.getEstablishment().getItemsHouse() != null) {
        	ItemsHouse items = cafeteria.getEstablishment().getItemsHouse();
        	if (items.isAccessibility()) {
        		lst.add(new StringInteger(17, getString(R.string.txt_acesso_deficientes), R.drawable.icn_acesso));
        	}
        	if (items.isCompressedAir()) {
        		lst.add(new StringInteger(18, getString(R.string.txt_tem_ar_condicionado), R.drawable.icn_ar_condicionado));
        	}
        	if (items.isWc()) {
        		lst.add(new StringInteger(19, getString(R.string.txt_tem_wc), R.drawable.icn_banheiros));
        	}
        	if (items.isSmoking()) {
        		lst.add(new StringInteger(20, getString(R.string.txt_tem_area_fumantes), R.drawable.icn_area_para_fumantes));
        	} else {
        		lst.add(new StringInteger(20, getString(R.string.txt_proibido_fumar), R.drawable.icn_proibido_fumar));
        	}
        	if (items.isParking()) {
        		lst.add(new StringInteger(21, getString(R.string.txt_tem_estacionamento), R.drawable.icn_estacionamento));
        	}
        	
        }
        if (lst.size() > 0) {
        	gridViewServicos.setExpanded(true);
            gridViewServicos.setAdapter(new ImageAdapter(this, lst));
        } else {
        	gridViewServicos.setVisibility(View.GONE);
        }
        
        dialogServicos2 = createServicos(lst);
		
	}
	
	private void initSlider(Photo[] photos) {
		if (photos != null && photos.length > 0) {
			for (Photo photo : photos) {
				
				GuiaCafeteriasApplication.imageLoader.loadImage(Photo.getUrlPhoto(photo), new ImageLoadingListener() {
					
					@Override
					public void onLoadingStarted(String imageUri, View view) {
					}
					
					@Override
					public void onLoadingFailed(String imageUri, View view,
							FailReason failReason) {
					}
					
					@Override
					public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
						File f = GuiaCafeteriasApplication.imageLoader.getDiskCache().get(imageUri);
						
						TextSliderView textSliderView = new TextSliderView(DetailStoreActivity.this);
						
				        textSliderView
				                .description("")
				                .image(f)
				                .setScaleType(BaseSliderView.ScaleType.Fit)
				                .setOnSliderClickListener(DetailStoreActivity.this);
			
			
				       mSlider.addSlider(textSliderView);
				       
				       findViewById(R.id.img_slider_default).setVisibility(View.GONE);
				       mSlider.setVisibility(View.VISIBLE);
					}
					
					@Override
					public void onLoadingCancelled(String imageUri, View view) {						
					}
				});
				
			}

	        if (photos != null && photos.length > 1) {
	        	mSlider.setIndicatorVisibility(IndicatorVisibility.Visible);
	        	//mSlider.setPresetTransformer(SliderLayout.Transformer.Background2Foreground);
	        	mSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
	        	mSlider.setCustomAnimation(new DescriptionAnimation());
	        	mSlider.setDuration(5000);
	 	        mSlider.startAutoCycle();
	        } else {
	        	mSlider.setIndicatorVisibility(IndicatorVisibility.Invisible);
	        }
		} else {
			findViewById(R.id.layout_slider).setVisibility(View.GONE);
		}
	}
	
	@Override
	public void onSliderClick(BaseSliderView slider) {
	}
	
    protected int getResourceMenu() {
 	   return R.menu.detail_store;   
    }
    
    @Override
    protected int hideMenu() {
    	return -1;
    }
    
    
    public static void showActivity(Context ctx, Cafeteria cafeteria) {
    	Intent i = new Intent(ctx, DetailStoreActivity.class);
    	i.putExtra(PARAM_INTENT_CAFETERIA, cafeteria);
    	ctx.startActivity(i);
    }
    
    @Override
    protected boolean homeAsUpEnabled() {
    	return true;
    }

	protected boolean customOpenClose() {
		return true;
	}
}
