package br.com.cafeeditora.guiadecafeterias.view.activity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.springframework.http.HttpMethod;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;

import br.com.cafeeditora.guiadecafeterias.R;
import br.com.cafeeditora.guiadecafeterias.model.Cafeteria;
import br.com.cafeeditora.guiadecafeterias.model.ResultCafeteria;
import br.com.cafeeditora.guiadecafeterias.util.Constants;
import br.com.cafeeditora.guiadecafeterias.util.Preferences;
import br.com.cafeeditora.guiadecafeterias.view.activity.adapter.CafeteriaMainAdapter;

import com.doisdoissete.android.util.ddsutil.gps.GpsUtil;
import com.doisdoissete.android.util.ddsutil.service.asynctask.ObserverAsyncTask;
import com.doisdoissete.android.util.ddsutil.service.asynctask.ServiceBuilder;
import com.doisdoissete.android.util.ddsutil.view.custom.listview.PagingListView;
import com.doisdoissete.android.util.ddsutil.view.custom.segmented.SegmentedGroup;
import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;
import com.nispok.snackbar.listeners.ActionClickListener;

public class ListCafeteriasActivity extends BaseActivity {

    private static final String PARAM_INTENT_TITULO = "PARAM_INTENT_TITULO";
    private static final String PARAM_INTENT_TIPO_TELA = "PARAM_INTENT_TIPO_TELA";
    private static final String PARAM_INTENT_URL = "PARAM_INTENT_URL";
    private static final int TIPO_TELA_TOP20 = 1;
    private static final int TIPO_TELA_FAVORITOS = 2;
    private static final int TIPO_TELA_REVELACAO = 3;
    private static final int TIPO_TELA_SEARCH = 4;

    private int titulo;
    private int tipoTela = TIPO_TELA_TOP20;
    private TextView txtTitle;
    private PagingListView lstView;
    private CafeteriaMainAdapter adapter;
    private List<Cafeteria> lstCafeterias;
    private int page = 1;
    private LinearLayout mQuickReturnView;
    private TranslateAnimation anim;
    private int actualVisibleItem = 0;
    private int translateYAnt = 0;

    private String ascDescOrder;
    private String orderBy;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.list_cafeterias_activity);

        if (getIntent() == null || getIntent().getExtras() == null && getIntent().getExtras().getInt(PARAM_INTENT_TITULO) == 0) {
            finish();
        }

        titulo = getIntent().getExtras().getInt(PARAM_INTENT_TITULO);
        tipoTela = getIntent().getExtras().getInt(PARAM_INTENT_TIPO_TELA);

        txtTitle = (TextView) findViewById(R.id.txt_title);
        lstView = (PagingListView) findViewById(R.id.list_view);
        mQuickReturnView = (LinearLayout) findViewById(R.id.layout_status_bar);

        txtTitle.setText(titulo);

        if (tipoTela == TIPO_TELA_SEARCH) {
            txtTitle.setVisibility(View.GONE);
        }
    }

    private boolean showStatusBar() {
        if (tipoTela == TIPO_TELA_TOP20 || tipoTela == TIPO_TELA_REVELACAO) {
            return true;
        }
        return false;
    }

    private void populateListView() {
        if (adapter == null) {
            lstView.setPagingableListener(new PagingListView.Pagingable() {
                @Override
                public void onLoadMoreItems() {
                    page++;
                    callService(orderBy, ascDescOrder);
                }
            });
            adapter = new CafeteriaMainAdapter(ListCafeteriasActivity.this, new ArrayList<Cafeteria>(), true);
            lstView.setAdapter(adapter);
            lstView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view,
                                        int position, long id) {
                    DetailStoreActivity.showActivity(ListCafeteriasActivity.this, adapter.getItem(position));
                }
            });
        }

        if (showStatusBar()) {
            initListViewStatus();
            mQuickReturnView.setVisibility(View.VISIBLE);
        }
        if (Integer.valueOf(Constants.TOTAL_ITENS_PAGE).intValue() > lstCafeterias.size()) {
            lstView.onFinishLoading(false, lstCafeterias);
        } else {
            lstView.onFinishLoading(true, lstCafeterias);
        }
        lstView.setVisibility(View.VISIBLE);
        adapter.notifyDataSetChanged();
        lstView.requestLayout();

    }

    @Override
    protected void onResume() {
        super.onResume();

        if ((adapter != null && adapter.getCount() <= 0) || adapter == null) {
            callService(null, null);
        } else if (tipoTela == TIPO_TELA_FAVORITOS) {
            if (Preferences.isFavDesfavClicked(ListCafeteriasActivity.this) ||
                    !Preferences.userIsLogado(ListCafeteriasActivity.this)) {
                refresh();
            }
            Preferences.setFavDesfavClicked(this, false);
        }
    }

    private void callService(String pOrderBy, String ascDesc) {
        this.ascDescOrder = ascDesc;
        this.orderBy = pOrderBy;
        mQuickReturnView.setVisibility(View.GONE);
        ObserverAsyncTask<ResultCafeteria> observer = new ObserverAsyncTask<ResultCafeteria>() {
            @Override
            public void onPreExecute() {
                SnackbarManager.dismiss();
                lstCafeterias = new ArrayList<Cafeteria>();
                if (page <= 1) {
                    if (adapter != null) {
                        adapter.removeAllItems();
                        adapter.notifyDataSetChanged();
                    }
                    lstView.scrollTo(0, 0);
                    lstView.smoothScrollToPosition(0);
                    lstView.setVisibility(View.GONE);
                    lstView.onFinishLoading(true, new ArrayList<Object>());
                    showWaitDialog();
                }
            }

            @Override
            public void onPostExecute(ResultCafeteria result) {
                if (result != null && result.getCafeterias() != null && result.getCafeterias().length > 0) {
                    lstCafeterias.addAll(Arrays.asList(result.getCafeterias()));

                    if (tipoTela == TIPO_TELA_TOP20 || tipoTela == TIPO_TELA_REVELACAO) {
                        for (Cafeteria caf : lstCafeterias) {
                            caf.setDistance(GpsUtil.distanceFloat(ListCafeteriasActivity.this, caf.getAddress().getGeolocation().getLatitude(), caf.getAddress().getGeolocation().getLongitude()));
                        }
                        if (TextUtils.isEmpty(orderBy)) {
                            Collections.sort(lstCafeterias);
                        }
                    }

                    populateListView();
                    dismissWaitDialog(true);
                    invalidateOptionsMenu();
                } else {
                    showNenhumaCafeteria();
                }
            }

            @Override
            public void onCancelled() {
                dismissWaitDialog(false);
                page--;
                if (page <= 0) {
                    page = 1;
                }
            }

            @Override
            public void onError(Exception e) {
                page--;
                if (page <= 0) {
                    page = 1;
                }
                showError(e);
            }
        };

        MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
        params.add(Constants.PARAM_PAGE, String.valueOf(page));

        ServiceBuilder sb = new ServiceBuilder();
        String url = Constants.QUERY_PATH;

        if (tipoTela == TIPO_TELA_TOP20) {
            url = Constants.QUERY_PATH_TOP_20;
        } else if (tipoTela == TIPO_TELA_REVELACAO) {
            url = Constants.QUERY_PATH_CAFETERIA_REVELACAO;
        } else if (tipoTela == TIPO_TELA_SEARCH) {
            url = getIntent().getExtras().getString(PARAM_INTENT_URL);
        } else if (tipoTela == TIPO_TELA_FAVORITOS) {
            url = Constants.FAVORITOS;
            if (!Preferences.userIsLogado(ListCafeteriasActivity.this)) {
                ActionClickListener actionClickListener = new ActionClickListener() {

                    @Override
                    public void onActionClicked(Snackbar snackbar) {
                        SettingsActivity.showActivity(ListCafeteriasActivity.this);
                    }
                };

                mSnackBar = Snackbar.with(ListCafeteriasActivity.this)
                        .text(R.string.txt_usuario_nao_loagado)
                        .actionLabel(R.string.txt_logar)
                        .actionListener(actionClickListener)
                        .duration(Snackbar.SnackbarDuration.LENGTH_INDEFINITE);
                SnackbarManager.show(mSnackBar);

                if (adapter != null) {
                    adapter.removeAllItems();
                    adapter.notifyDataSetChanged();
                }

                return;
            }
        }
        params.add(Cafeteria.PARAM_ACCESS_TOKEN, Preferences.getToken(ListCafeteriasActivity.this));
        if (!TextUtils.isEmpty(orderBy)) {
            params.add(Constants.PARAM_ORDER, ascDescOrder);
            params.add(Constants.PARAM_ORDER_BY, orderBy);
        } else {
            params.add(Constants.PARAM_ORDER, "");
            params.add(Constants.PARAM_ORDER_BY, "");
        }

        sb.setUrl(url)
                .setObserverAsyncTask(observer)
                .setNeedConnection(true)
                .setHttpMethod(HttpMethod.GET)
                .setParams(params);

        async = sb.mappingInto(ListCafeteriasActivity.this, ResultCafeteria.class);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            async.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            async.execute();
        }
    }

    @Override
    protected void refresh() {
        super.refresh();
        lstView.setVisibility(View.GONE);
        callService(orderBy, ascDescOrder);
    }

    private void showNenhumaCafeteria() {
        dismissWaitDialog(false);

        if (page <= 1) {
            if (tipoTela == TIPO_TELA_FAVORITOS) {
                showSimpleSnack(R.string.txt_msg_nenhuma_cafeteria_favorita, Snackbar.SnackbarDuration.LENGTH_INDEFINITE);
            } else {
                showSnack(R.string.msg_nenhum_resultado);
            }

            lstView.setVisibility(View.GONE);
        }

        lstCafeterias = new ArrayList<Cafeteria>();
        lstView.onFinishLoading(false, new ArrayList<Object>());
    }

    @Override
    protected int hideMenu() {
        int tipo = getIntent().getExtras().getInt(PARAM_INTENT_TIPO_TELA);
        if (TIPO_TELA_FAVORITOS == tipo) {
            return R.id.action_favoritos;
        } else if (TIPO_TELA_REVELACAO == tipo) {
            return R.id.action_cafeterias_revelacao;
        } else if (TIPO_TELA_SEARCH == tipo) {
            return -1;
        } else if (TIPO_TELA_TOP20 == tipo) {
            return R.id.action_top_20;
        }

        return -1;

    }

    protected int getResourceMenu() {
        int tipo = getIntent().getExtras().getInt(PARAM_INTENT_TIPO_TELA);
        if (TIPO_TELA_SEARCH == tipo) {
            return -1;
        }
        return super.getResourceMenu();
    }


    public static void showTop20Activity(Context ctx) {
        Intent it = new Intent(ctx, ListCafeteriasTop20Activity.class);
        it.putExtra(PARAM_INTENT_TITULO, R.string.txt_top_20_title);
        it.putExtra(PARAM_INTENT_TIPO_TELA, TIPO_TELA_TOP20);
        ctx.startActivity(it);
    }

    public static void showRevelacaoActivity(Context ctx) {
        Intent it = new Intent(ctx, ListCafeteriasRevelacaoActivity.class);
        it.putExtra(PARAM_INTENT_TITULO, R.string.txt_cafeterias_revelacao);
        it.putExtra(PARAM_INTENT_TIPO_TELA, TIPO_TELA_REVELACAO);
        ctx.startActivity(it);
    }

    public static void showFavoritosActivity(Context ctx) {
        Intent it = new Intent(ctx, ListCafeteriasFavoritosActivity.class);
        it.putExtra(PARAM_INTENT_TITULO, R.string.txt_favoritos);
        it.putExtra(PARAM_INTENT_TIPO_TELA, TIPO_TELA_FAVORITOS);
        ctx.startActivity(it);
    }

    public static void showSearchResultActivity(Context ctx, String url) {
        Intent it = new Intent(ctx, ListCafeteriasSearchActivity.class);
        it.putExtra(PARAM_INTENT_TITULO, R.string.txt_resultado_de_busca);
        it.putExtra(PARAM_INTENT_TIPO_TELA, TIPO_TELA_SEARCH);
        it.putExtra(PARAM_INTENT_URL, url);
        ctx.startActivity(it);
    }

    @Override
    protected boolean homeAsUpEnabled() {
        int tipo = getIntent().getExtras().getInt(PARAM_INTENT_TIPO_TELA);
        if (TIPO_TELA_SEARCH == tipo) {
            return true;
        }
        return false;

    }

    private void animateStatus(int translationY) {
        mQuickReturnView.clearAnimation();
        anim = new TranslateAnimation(0, 0, translateYAnt, translationY);
        anim.setFillAfter(true);
        anim.setDuration(500);
        mQuickReturnView.startAnimation(anim);
        translateYAnt = translationY;
    }

    private void initListViewStatus() {
        initSegmentedControl();
        lstView.setOnScrollListener(new AbsListView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {
                if (actualVisibleItem < firstVisibleItem) {
                    animateStatus(mQuickReturnView.getHeight());
                } else if (actualVisibleItem > firstVisibleItem) {
                    animateStatus(0);
                }
                actualVisibleItem = firstVisibleItem;
            }
        });

    }

    private void initSegmentedControl() {
        SegmentedGroup segmented = (SegmentedGroup) findViewById(R.id.seg_order);
        segmented.setTintColor(Color.BLACK);
        segmented.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.bt_order_distancia) {
                    page = 1;
                    callService(null, null);
                } else {
                    page = 1;
                    callService("ratings.average", "DESC");
                }
            }
        });
    }

    protected boolean customOpenClose() {
        return true;
    }
}
