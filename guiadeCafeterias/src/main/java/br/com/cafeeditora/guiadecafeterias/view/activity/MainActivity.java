package br.com.cafeeditora.guiadecafeterias.view.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;

import com.doisdoissete.android.util.ddsutil.service.asynctask.ObserverAsyncTask;
import com.doisdoissete.android.util.ddsutil.service.asynctask.ServiceBuilder;
import com.doisdoissete.android.util.ddsutil.view.custom.listview.PagingListView;
import com.doisdoissete.android.util.ddsutil.view.custom.segmented.SegmentedGroup;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.nispok.snackbar.SnackbarManager;

import org.springframework.http.HttpMethod;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import br.com.cafeeditora.guiadecafeterias.R;
import br.com.cafeeditora.guiadecafeterias.model.Cafeteria;
import br.com.cafeeditora.guiadecafeterias.model.ResultCafeteria;
import br.com.cafeeditora.guiadecafeterias.util.ColorStateUtil;
import br.com.cafeeditora.guiadecafeterias.util.Constants;
import br.com.cafeeditora.guiadecafeterias.util.Preferences;
import br.com.cafeeditora.guiadecafeterias.view.activity.adapter.CafeteriaMainAdapter;
import br.com.cafeeditora.guiadecafeterias.view.activity.adapter.MarkerAdapter;

public class MainActivity extends BaseActivity {

    private PagingListView lstView;
    private CafeteriaMainAdapter adapter;
    private boolean showMap = false;
    private GoogleMap mMap;
    private SupportMapFragment mMapFragment;
    private List<Cafeteria> lstCafeterias;

    private LinearLayout mQuickReturnView;

    private TranslateAnimation anim;
    private int actualVisibleItem = 0;
    private int translateYAnt = 0;

    private HashMap<Marker, Cafeteria> markers = new HashMap<Marker, Cafeteria>();
    private Marker actualMarker = null;
    private int page = 1;
    private Location location;
    private String ascDescOrder;
    private String orderBy;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);

        lstView = (PagingListView) findViewById(R.id.list_view);

        mQuickReturnView = (LinearLayout) findViewById(R.id.layout_status_bar);
        lstView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                DetailStoreActivity.showActivity(MainActivity.this, adapter.getItem(position));
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
            }
        });

        initMap();

        showLista();
        //showMap();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (adapter == null || (adapter != null && adapter.getCount() <= 0)) {
            callService(null, null);
        }
    }

    private void initMap() {
        mMapFragment = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map));
        mMap = mMapFragment.getMap();
        mMap.getUiSettings().setZoomControlsEnabled(false);
        mMap.setMyLocationEnabled(true);

        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {

            @Override
            public void onInfoWindowClick(Marker marker) {
                Cafeteria cafeteria = markers.get(marker);
                DetailStoreActivity.showActivity(MainActivity.this, cafeteria);
            }
        });
    }

    private void animateStatus(int translationY) {
        mQuickReturnView.clearAnimation();
        anim = new TranslateAnimation(0, 0, translateYAnt, translationY);
        anim.setFillAfter(true);
        anim.setDuration(500);
        mQuickReturnView.startAnimation(anim);
        translateYAnt = translationY;
    }

    private void initListViewStatus() {

        lstView.setOnScrollListener(new AbsListView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {
                if (actualVisibleItem < firstVisibleItem) {
                    animateStatus(mQuickReturnView.getHeight());
                } else if (actualVisibleItem > firstVisibleItem) {
                    animateStatus(0);
                }
                actualVisibleItem = firstVisibleItem;
            }
        });
    }

    private void initSegmentedControl(String state) {
        SegmentedGroup segmented = (SegmentedGroup) findViewById(R.id.seg_order);
        segmented.setTintColor(Color.BLACK);
        segmented.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.bt_order_distancia) {
                    page = 1;
                    callService(null, null);
                } else {
                    page = 1;
                    callService("ratings.average", "DESC");
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        boolean result = super.onCreateOptionsMenu(menu);
        if (lstCafeterias != null && lstCafeterias.size() > 0) {
            if (showMap) {
                menu.findItem(R.id.action_lista).setVisible(true);
                menu.findItem(R.id.action_mapa).setVisible(false);
            } else {
                menu.findItem(R.id.action_lista).setVisible(false);
                menu.findItem(R.id.action_mapa).setVisible(true);
            }
        } else {
            menu.findItem(R.id.action_lista).setVisible(false);
            menu.findItem(R.id.action_mapa).setVisible(false);
        }

        return result;
    }

    protected int getResourceMenu() {
        return R.menu.home;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_lista) {
            showLista();
            return true;
        } else if (id == R.id.action_mapa) {
            showMap();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected int hideMenu() {
        return R.id.action_cafeterias;
    }

    private void showMap() {
        showMap = true;
        invalidateOptionsMenu();
        lstView.setVisibility(View.GONE);
        mMapFragment.getView().setVisibility(View.VISIBLE);
        mQuickReturnView.clearAnimation();
        mQuickReturnView.setVisibility(View.GONE);
    }

    private void showLista() {
        showMap = false;
        invalidateOptionsMenu();
        lstView.setVisibility(View.VISIBLE);
        mMapFragment.getView().setVisibility(View.GONE);
        mQuickReturnView.setVisibility(View.VISIBLE);
        animateStatus(mQuickReturnView.getHeight());
    }

    public static void showActivity(Context ctx) {
        Intent i = new Intent(ctx, MainActivity.class);
        ctx.startActivity(i);

    }

    protected boolean customOpenClose() {
        return true;
    }

    private void callService(String order, String ascDesc) {
        this.ascDescOrder = ascDesc;
        this.orderBy = order;
        if (verifyGpsEnable()) {
            ObserverAsyncTask<ResultCafeteria> observer = new ObserverAsyncTask<ResultCafeteria>() {
                @Override
                public void onPreExecute() {
                    SnackbarManager.dismiss();
                    lstCafeterias = new ArrayList<Cafeteria>();
                    if (page <= 1) {
                        if (adapter != null) {
                            adapter.removeAllItems();
                            adapter.notifyDataSetChanged();

                        }
                        lstView.scrollTo(0, 0);
                        lstView.smoothScrollToPosition(0);
                        mMap.clear();
                        showWaitDialog();
                    }
                }

                @Override
                public void onPostExecute(ResultCafeteria result) {
                    if (result != null && result.getCafeterias() != null && result.getCafeterias().length > 0) {

                        lstCafeterias.addAll(Arrays.asList(result.getCafeterias()));

                        if (page == 1 && result.getCafeterias()[0].getAddress() != null) {
                            initSegmentedControl(getString(result.getCafeterias()[0].getAddress().getStateToCompare()));
                        }

                        populateListView();
                        populateMap();

                        dismissWaitDialog(true);
                        invalidateOptionsMenu();
                    } else {
                        showNenhumaCafeteria();
                    }
                }

                @Override
                public void onCancelled() {
                    dismissWaitDialog(false);
                    page--;
                    if (page <= 0) {
                        page = 1;
                    }
                }

                @Override
                public void onError(Exception e) {
                    page--;
                    if (page <= 0) {
                        page = 1;
                    }
                    showError(e);
                }
            };

            MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
            params.add(Cafeteria.PARAM_ACCESS_TOKEN, Preferences.getToken(MainActivity.this));
            params.add(Constants.PARAM_PAGE, String.valueOf(page));

            if (location == null) {
                location = gps.getLocation();
            }
            if (location == null) {
                showSnackHabilitarGps();
                return;
            }

            String url = Constants.QUERY_PATH_GEOLOCATION;

            if (!TextUtils.isEmpty(this.orderBy)) {
                params.add(Constants.PARAM_ORDER, this.ascDescOrder);
                params.add(Constants.PARAM_ORDER_BY, this.orderBy);
                params.add(Constants.PARAM_GEOLOCATION_LAT, "");
                params.add(Constants.PARAM_GEOLOCATION_LNG, "");
                url = Constants.QUERY_PATH_GEOLOCATION_WITHOUT_LOCATION;
            } else {
                params.add(Constants.PARAM_ORDER, "");
                params.add(Constants.PARAM_ORDER_BY, "");
                params.add(Constants.PARAM_GEOLOCATION_LAT, String.valueOf(location.getLatitude()));
                params.add(Constants.PARAM_GEOLOCATION_LNG, String.valueOf(location.getLongitude()));
            }

            ServiceBuilder sb = new ServiceBuilder();
            sb.setUrl(url)
                    .setObserverAsyncTask(observer)
                    .setNeedConnection(true)
                    .setHttpMethod(HttpMethod.GET)
                    .setParams(params);

            async = sb.mappingInto(MainActivity.this, ResultCafeteria.class);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                async.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else {
                async.execute();
            }
        } else {
            dismissWaitDialog(false);
        }
    }

    @Override
    protected void gpsEnable(Location pLocation) {
        super.gpsEnable(pLocation);
        this.location = pLocation;
        refresh();
    }

    private void showNenhumaCafeteria() {
        dismissWaitDialog(false);
        showSnack(R.string.msg_nenhum_resultado);
    }

    private void populateListView() {
        if (adapter == null) {
            initListViewStatus();
            lstView.setPagingableListener(new PagingListView.Pagingable() {
                @Override
                public void onLoadMoreItems() {
                    page++;
                    callService(orderBy, ascDescOrder);
                }
            });
            adapter = new CafeteriaMainAdapter(MainActivity.this, lstCafeterias, true);
            lstView.setAdapter(adapter);
            lstView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view,
                                        int position, long id) {
                    DetailStoreActivity.showActivity(MainActivity.this, adapter.getItem(position));
                }
            });
        }

        if (Integer.valueOf(Constants.TOTAL_ITENS_PAGE).intValue() > lstCafeterias.size()) {
            lstView.onFinishLoading(false, lstCafeterias);
        } else {
            lstView.onFinishLoading(true, lstCafeterias);
        }
        lstView.setVisibility(View.VISIBLE);
        adapter.notifyDataSetChanged();
        lstView.requestLayout();
    }

    private void populateMap() {

        markers = new HashMap<Marker, Cafeteria>();
        LatLng loc = null;
        for (Cafeteria obj : lstCafeterias) {
            if (obj.getAddress() != null && obj.getAddress().getGeolocation() != null
                    && obj.getAddress().getGeolocation().getLatitude() != null
                    && obj.getAddress().getGeolocation().getLongitude() != null) {

                if (loc == null) {
                    if (location == null) {
                        loc = new LatLng(obj.getAddress().getGeolocation().getLatitude(), obj.getAddress().getGeolocation().getLongitude());
                    } else {
                        loc = new LatLng(location.getLatitude(), location.getLongitude());
                    }

                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(loc, 14.0f));
                }
                Marker m = mMap.addMarker(new MarkerOptions()
                        .icon(BitmapDescriptorFactory.fromResource(ColorStateUtil.getMapIcon(getString(obj.getAddress().getStateToCompare()))))
                        .position(new LatLng(obj.getAddress().getGeolocation().getLatitude(), obj.getAddress().getGeolocation().getLongitude()))
                        .title(obj.getName())
                        .draggable(true)
                        .flat(true)
                        .snippet(obj.getFormatedAddress()));

                markers.put(m, obj);
            }

        }

        View layoutMarker = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.cafeteria_marker, null);

        mMap.setInfoWindowAdapter(new MarkerAdapter(null, MainActivity.this, markers, layoutMarker));
    }

    @Override
    protected void refresh() {
        super.refresh();
        callService(orderBy, ascDescOrder);
    }

    @Override
    protected boolean homeAsUpEnabled() {
        return false;
    }
}
