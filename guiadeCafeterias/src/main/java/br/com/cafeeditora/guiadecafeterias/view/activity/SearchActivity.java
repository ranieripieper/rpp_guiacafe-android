package br.com.cafeeditora.guiadecafeterias.view.activity;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;

import com.doisdoissete.android.util.ddsutil.service.asynctask.AsyncTaskService;
import com.doisdoissete.android.util.ddsutil.service.asynctask.ObserverAsyncTask;
import com.doisdoissete.android.util.ddsutil.service.asynctask.ServiceBuilder;
import com.doisdoissete.android.util.ddsutil.view.EditTextUtil;
import com.doisdoissete.android.util.ddsutil.view.custom.TextViewPlus;

import org.springframework.http.HttpMethod;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.ArrayList;
import java.util.List;

import br.com.cafeeditora.guiadecafeterias.R;
import br.com.cafeeditora.guiadecafeterias.model.EstadoCidadeBairro;
import br.com.cafeeditora.guiadecafeterias.util.Constants;
import br.com.cafeeditora.guiadecafeterias.util.Preferences;
import br.com.cafeeditora.guiadecafeterias.view.activity.adapter.SearchAdapter;
import br.com.cafeeditora.guiadecafeterias.view.activity.adapter.model.SearchAdapterModel;

public class SearchActivity extends BaseActivity {

    private ListView lstView;
    private LayoutInflater inflater;
    private SearchAdapter searchAdapter;
    private EditText edtBuscar;
    private TextViewPlus txtEstado;
    private TextViewPlus txtCidade;
    private TextViewPlus txtBairro;
    private String[] estados;
    private String[] cidades;
    private String[] bairros;

    private String estadoSelect;
    private String cidadeSelect;
    private String bairroSelect;
    private boolean autoSelectOneReg = true;

    private View loading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        setContentView(R.layout.search_activity);
        lstView = (ListView) findViewById(R.id.list_view);

        lstView.addHeaderView(getHeaderView());
        lstView.addFooterView(getFooterView());

        List<SearchAdapterModel> lst = new ArrayList<SearchAdapterModel>();
        SearchAdapterModel m = new SearchAdapterModel("wifi", R.drawable.icn_wifi_search, R.string.txt_tem_wifi, false);
        lst.add(m);
        m = new SearchAdapterModel("attested", R.drawable.icn_certificado_search, R.string.txt_tem_barista, false);
        lst.add(m);
        m = new SearchAdapterModel("tasting", R.drawable.icn_degustacao_search, R.string.txt_oferece_degustacao, false);
        lst.add(m);
        m = new SearchAdapterModel("roasting", R.drawable.icn_torra_cafe_search, R.string.txt_torra_cafe, false);
        lst.add(m);
        m = new SearchAdapterModel("tnt", R.drawable.icn_tnt_search, R.string.txt_campeonato_tnt, false);
        lst.add(m);
        m = new SearchAdapterModel("closeTouristPoint", R.drawable.icn_turistico_search, R.string.txt_prox_ponto_turistico, false);
        lst.add(m);
        m = new SearchAdapterModel("closeMetroPoint", R.drawable.icn_turistico_search, R.string.txt_prox_estacao_metro, false);
        lst.add(m);
        m = new SearchAdapterModel("liveMusic", R.drawable.icn_musica_search, R.string.txt_tem_musica, false);
        lst.add(m);
        m = new SearchAdapterModel("openAir", R.drawable.icn_area_ar_livre_search, R.string.txt_tem_area_ar_livre, false);
        lst.add(m);
        m = new SearchAdapterModel("decaffeinated", R.drawable.icn_decafeinado_search, R.string.txt_oferece_cafe_descafeinado, false);
        lst.add(m);
        m = new SearchAdapterModel("hasAlternateTypes", R.drawable.icn_restricoes_search, R.string.txt_restricoes_alimentares, false);
        lst.add(m);
        m = new SearchAdapterModel("courses", R.drawable.icn_cursos_search, R.string.txt_oferece_cursos, false);
        lst.add(m);
        m = new SearchAdapterModel("hasAwards", R.drawable.icn_premiado_search, R.string.txt_tem_premios, false);
        lst.add(m);
        m = new SearchAdapterModel("toGoBuy", R.drawable.icn_para_levar_search, R.string.txt_sair_tomando, false);
        lst.add(m);
        m = new SearchAdapterModel("toBuy", R.drawable.icn_presente_search, R.string.txt_cafe_graos_moido, false);
        lst.add(m);
        m = new SearchAdapterModel("check", R.drawable.icn_cheque_search, R.string.txt_aceita_cheque, false);
        lst.add(m);
        m = new SearchAdapterModel("accessibility", R.drawable.icn_acesso_search, R.string.txt_acesso_deficientes, false);
        lst.add(m);
        m = new SearchAdapterModel("compressedAir", R.drawable.icn_ar_condicionado_search, R.string.txt_tem_ar_condicionado, false);
        lst.add(m);
        m = new SearchAdapterModel("wc", R.drawable.icn_banheiros_search, R.string.txt_tem_wc, false);
        lst.add(m);
        m = new SearchAdapterModel("smoking", R.drawable.icn_area_para_fumantes_search, R.string.txt_tem_area_fumantes, false);
        lst.add(m);
        m = new SearchAdapterModel("parking", R.drawable.icn_estacionamento_search, R.string.txt_tem_estacionamento, false);
        lst.add(m);

        searchAdapter = new SearchAdapter(this, lst);
        lstView.setAdapter(searchAdapter);

        edtBuscar.setOnKeyListener(new View.OnKeyListener() {

            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    EditTextUtil.hiddenKeyboard(SearchActivity.this, edtBuscar);
                }
                return false;
            }
        });

        txtEstado.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StateCityDistrictDialogFragment stateCityDistrictDialogFragment = StateCityDistrictDialogFragment.newInstance(estados, estadoSelect, new StateCityDistrictDialogFragment.SelectItemListener() {
                    @Override
                    public void onSelect(String item) {
                        onEstadoSelect(item);
                    }
                });
                stateCityDistrictDialogFragment.show(getSupportFragmentManager(), "estado");

            }
        });

        txtCidade.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StateCityDistrictDialogFragment stateCityDistrictDialogFragment = StateCityDistrictDialogFragment.newInstance(cidades, cidadeSelect, new StateCityDistrictDialogFragment.SelectItemListener() {
                    @Override
                    public void onSelect(String item) {
                        onCidadeSelect(item);
                    }
                });
                stateCityDistrictDialogFragment.show(getSupportFragmentManager(), "cidades");

            }
        });

        txtBairro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StateCityDistrictDialogFragment stateCityDistrictDialogFragment = StateCityDistrictDialogFragment.newInstance(bairros, bairroSelect, new StateCityDistrictDialogFragment.SelectItemListener() {
                    @Override
                    public void onSelect(String item) {
                        onBairroSelect(item);
                    }
                });
                stateCityDistrictDialogFragment.show(getSupportFragmentManager(), "bairros");
            }
        });

        estados = Preferences.getEstados(this);
        if (estados == null || estados.length <= 0) {
            estados = getResources().getStringArray(R.array.estados);
        }
        loadEstados();
    }

    private void onEstadoSelect(String value) {
        hideView(txtBairro);
        hideView(txtCidade);
        onBairroSelect(null);
        onCidadeSelect(null);
        if (TextUtils.isEmpty(value)) {
            txtEstado.setText(R.string.selecionar_estado);
            estadoSelect = null;
        } else {
            txtEstado.setText(getString(R.string.estado, value));
            estadoSelect = value;
            loadCidades();
        }
    }

    private void onCidadeSelect(String value) {
        hideView(txtBairro);
        onBairroSelect(null);
        if (TextUtils.isEmpty(value)) {
            cidadeSelect = null;
            txtCidade.setText(R.string.selecionar_cidade);
        } else {
            txtCidade.setText(getString(R.string.cidade, value));
            cidadeSelect = value;
            loadBairros();
        }
    }

    private void onBairroSelect(String value) {
        if (TextUtils.isEmpty(value)) {
            bairroSelect = null;
            txtBairro.setText(R.string.selecionar_bairro);
        } else {
            txtBairro.setText(getString(R.string.bairro, value));
            bairroSelect = value;
        }
    }

    private void showCidades() {
        showView(txtCidade);
    }

    private void showBairros() {
        showView(txtBairro);
    }

    private void showView(View v) {
        //v.setAlpha(0f);
        v.setVisibility(View.VISIBLE);
        //v.animate().alpha(1.0f).setDuration(400).start();
    }

    private void hideView(final View v) {
        v.setVisibility(View.GONE);
    }

    private void callSearch() {
        String url = Constants.QUERY_PATH;
        String params = "";
        if (!TextUtils.isEmpty(edtBuscar.getText())) {
            params += String.format("&%s=%s", Constants.PARAM_TEXT_SEARCH, edtBuscar.getText());
        }

        //monta parâmetros do adapter
        for (int i = 0; i < searchAdapter.getCount(); i++) {
            SearchAdapterModel model = searchAdapter.getItem(i);
            if (model.isChecked()) {
                params += String.format("&%s=%s", Constants.PARAM_FILTERS, model.getParamUrl());
            }
        }

        if (!TextUtils.isEmpty(estadoSelect)) {
            params += String.format("&%s=%s", Constants.PARAM_STATE, estadoSelect);
        }
        if (!TextUtils.isEmpty(cidadeSelect)) {
            params += String.format("&%s=%s", Constants.PARAM_CITY, cidadeSelect);
        }
        if (!TextUtils.isEmpty(bairroSelect)) {
            params += String.format("&%s=%s", Constants.PARAM_DISTRICT, bairroSelect);
        }

        if (params.length() > 0) {
            params = params.substring(1, params.length());
        }

        url += "&" + params;
        ListCafeteriasActivity.showSearchResultActivity(SearchActivity.this, url);
    }

    private View getHeaderView() {
        View vi = inflater.inflate(R.layout.include_header_search, null);
        edtBuscar = (EditText) vi.findViewById(R.id.edt_buscar);
        txtEstado = (TextViewPlus) vi.findViewById(R.id.txt_estado);
        txtCidade = (TextViewPlus) vi.findViewById(R.id.txt_cidade);
        txtBairro = (TextViewPlus) vi.findViewById(R.id.txt_bairro);
        loading = vi.findViewById(R.id.loading);
        return vi;
    }

    private View getFooterView() {
        View vi = inflater.inflate(R.layout.include_footer_search, null);

        vi.findViewById(R.id.bt_search).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                callSearch();
            }
        });

        return vi;
    }

    private void loadEstados() {

        ObserverAsyncTask<EstadoCidadeBairro> observer = new ObserverAsyncTask<EstadoCidadeBairro>() {
            @Override
            public void onPreExecute() {
            }

            @Override
            public void onPostExecute(EstadoCidadeBairro result) {
                if (result != null && result.getData() != null && result.getData().length > 0) {
                    Preferences.setEstados(SearchActivity.this, result.getData());
                    estados = Preferences.getEstados(SearchActivity.this);
                }
            }

            @Override
            public void onCancelled() {
            }

            @Override
            public void onError(Exception e) {
            }
        };

        loadEstadoCidadeBairro(null, null, observer);

    }

    private void loadCidades() {

        ObserverAsyncTask<EstadoCidadeBairro> observer = new ObserverAsyncTask<EstadoCidadeBairro>() {
            @Override
            public void onPreExecute() {
                loading.setVisibility(View.VISIBLE);
            }

            @Override
            public void onPostExecute(EstadoCidadeBairro result) {
                loading.setVisibility(View.GONE);
                if (result != null && result.getData() != null && result.getData().length > 0) {
                    cidades = result.getData();
                    if (autoSelectOneReg && cidades.length == 1) {
                        onCidadeSelect(cidades[0]);
                    }
                    showCidades();
                }
            }

            @Override
            public void onCancelled() {
                loading.setVisibility(View.GONE);
            }

            @Override
            public void onError(Exception e) {
                loading.setVisibility(View.GONE);
            }
        };

        loadEstadoCidadeBairro(estadoSelect, null, observer);

    }

    private void loadBairros() {

        ObserverAsyncTask<EstadoCidadeBairro> observer = new ObserverAsyncTask<EstadoCidadeBairro>() {
            @Override
            public void onPreExecute() {
                loading.setVisibility(View.VISIBLE);
            }

            @Override
            public void onPostExecute(EstadoCidadeBairro result) {
                loading.setVisibility(View.GONE);
                if (result != null && result.getData() != null && result.getData().length > 0) {
                    bairros = result.getData();
                    if (autoSelectOneReg && bairros.length == 1) {
                        onBairroSelect(bairros[0]);
                    }
                    showCidades();

                    showBairros();
                }
            }

            @Override
            public void onCancelled() {
                loading.setVisibility(View.GONE);
            }

            @Override
            public void onError(Exception e) {
                loading.setVisibility(View.GONE);
            }
        };

        loadEstadoCidadeBairro(estadoSelect, cidadeSelect, observer);

    }

    //servicos
    private void loadEstadoCidadeBairro(String estado, String cidade, ObserverAsyncTask<EstadoCidadeBairro> observer) {

        MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();

        String url = Constants.STATE_CITIES_DISTRICTS;

        params.add(Constants.PARAM_STATE, estado != null ? estado : "");
        params.add(Constants.PARAM_CITY, cidade != null ? cidade : "");

        ServiceBuilder sb = new ServiceBuilder();
        sb.setUrl(url)
                .setObserverAsyncTask(observer)
                .setNeedConnection(true)
                .setHttpMethod(HttpMethod.GET)
                .setParams(params);

        AsyncTaskService async = sb.mappingInto(SearchActivity.this, EstadoCidadeBairro.class);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            async.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            async.execute();
        }
    }

    public static void showActivity(Context ctx) {
        Intent it = new Intent(ctx, SearchActivity.class);
        ctx.startActivity(it);
    }

    @Override
    protected int hideMenu() {
        return R.id.action_busca_de_cafeteria;
    }

    @Override
    protected boolean homeAsUpEnabled() {
        return false;
    }

    protected boolean customOpenClose() {
        return true;
    }
}
