package br.com.cafeeditora.guiadecafeterias.view.activity;

import android.content.Context;
import android.content.Intent;
import android.content.IntentSender.SendIntentException;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.doisdoissete.android.util.ddsutil.service.asynctask.ObserverAsyncTask;
import com.doisdoissete.android.util.ddsutil.service.asynctask.ServiceBuilder;
import com.doisdoissete.android.util.ddsutil.view.custom.util.FontUtilCache;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphUser;
import com.facebook.widget.LoginButton;
import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.auth.UserRecoverableAuthException;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;

import org.springframework.http.HttpMethod;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import br.com.cafeeditora.guiadecafeterias.R;
import br.com.cafeeditora.guiadecafeterias.model.ResultLogin;
import br.com.cafeeditora.guiadecafeterias.util.Constants;
import br.com.cafeeditora.guiadecafeterias.util.Preferences;

public class SettingsActivity extends BaseActivity implements ConnectionCallbacks, OnConnectionFailedListener {


    private static final String TAG = "SettingsActivity";

    private Button btnConectarGoogle;
    private LoginButton btnConectarFacebook;
    private RetrieveTokenTask retrieveTokenTask;

    private int RECOVERABLE_REQUEST_CODE = 997;

    /* Track whether the sign-in button has been clicked so that we know to resolve
     * all issues preventing sign-in without waiting.
     */
    private boolean mSignInClicked;
    /* Store the connection result from onConnectionFailed callbacks so that we can
     * resolve them when the user clicks sign-in.
     */
    private ConnectionResult mConnectionResult;
    /* Request code used to invoke sign in user interactions. */
    private static final int RC_SIGN_IN = 0;

    /* Client used to interact with Google APIs. */
    private GoogleApiClient mGoogleApiClient;

    /* A flag indicating that a PendingIntent is in progress and prevents
    * us from starting further intents.
    */
    private boolean mIntentInProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.settings_activity);
        initGooglePlus();
        initFacebook(savedInstanceState);

    }

    @Override
    protected void onResume() {
        super.onResume();
        uiHelper.onResume();
    }

    private void initGooglePlus() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Plus.API)
                .addScope(Plus.SCOPE_PLUS_LOGIN)
                .build();

        btnConectarGoogle = (Button) findViewById(R.id.btn_conectar_google);
        findViewById(R.id.img_google).setOnClickListener(googlePlusClick);
        btnConectarGoogle.setOnClickListener(googlePlusClick);

        if (mGoogleApiClient.isConnected()) {
            btnConectarGoogle.setBackgroundResource(R.drawable.bg_button_g_connected);
        } else {
            btnConectarGoogle.setBackgroundResource(R.drawable.bg_button);
        }
    }

    private void callLoginCadastroService(final String redeSocial, final String name, final String email, final String token, final String fbId) {
        ObserverAsyncTask<ResultLogin> observer = new ObserverAsyncTask<ResultLogin>() {
            @Override
            public void onPreExecute() {
                showWaitDialog();
            }

            @Override
            public void onPostExecute(ResultLogin result) {
                if (result != null && result.isSuccess() && result.getData() != null
                        && result.getData().length > 0 && !TextUtils.isEmpty(result.getData()[0].getAccessToken())) {

                    if (redeSocial.equals(Constants.GOOGLE_PLUS)) {
                        btnConectarGoogle.setBackgroundResource(R.drawable.bg_button_g_connected);
                        callFacebookLogout(SettingsActivity.this);
                    } else if (redeSocial.equals(Constants.FACEBOOK)) {
                        btnConectarFacebook.setBackgroundResource(R.drawable.bg_button_fb_connected);
                        disconnectGooglePlus();
                    }
                    Preferences.setToken(SettingsActivity.this, result.getData()[0].getAccessToken());
                    dismissWaitDialog(true);
                } else {
                    showError(null, true);
                    disconnect();
                }
            }

            @Override
            public void onCancelled() {
                dismissWaitDialog(false);
                disconnect();
            }

            @Override
            public void onError(Exception e) {
                showError(e, true);
                disconnect();
            }
        };

        MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
        params.add(ResultLogin.PARAM_EMAIL, email);
        params.add(ResultLogin.PARAM_NAME, name);
        params.add(ResultLogin.PARAM_SOURCE, redeSocial);
        params.add(ResultLogin.PARAM_TOKEN, token);
        params.add(ResultLogin.PARAM_FACEBOOK_ID, fbId);


        ServiceBuilder sb = new ServiceBuilder();
        sb.setUrl(Constants.LOGIN)
                .setObserverAsyncTask(observer)
                .setNeedConnection(true)
                .setHttpMethod(HttpMethod.POST)
                .setParams(params);

        async = sb.mappingInto(SettingsActivity.this, ResultLogin.class);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            async.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            async.execute();
        }
    }

    private void disconnect() {
        disconnectGooglePlus();
        Preferences.setToken(SettingsActivity.this, "");
        callFacebookLogout(SettingsActivity.this);
    }

    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    protected void onStop() {
        super.onStop();

        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        uiHelper.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (retrieveTokenTask != null) {
            retrieveTokenTask.cancel(true);
        }
        if (uiHelper != null) {
            uiHelper.onDestroy();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        uiHelper.onSaveInstanceState(outState);
    }

    protected void onActivityResult(int requestCode, int responseCode,
                                    Intent intent) {

        //Google +
        if (requestCode == RC_SIGN_IN) {
            if (responseCode != RESULT_OK) {
                mSignInClicked = false;
            }

            mIntentInProgress = false;

            if (!mGoogleApiClient.isConnecting()) {
                mGoogleApiClient.connect();
            }
        } else if (requestCode == RECOVERABLE_REQUEST_CODE && responseCode == RESULT_OK) {
            /*Bundle extra = intent.getExtras();
	        String oneTimeToken = extra.getString("authtoken");*/
        } else {
            uiHelper.onActivityResult(requestCode, responseCode, intent);
        }
    }

    public static void showActivity(Context ctx) {
        Intent it = new Intent(ctx, SettingsActivity.class);
        ctx.startActivity(it);
    }

    // Google Plus
	/* A helper method to resolve the current ConnectionResult error. */
    private void resolveSignInError() {
        if (mConnectionResult != null && mConnectionResult.hasResolution()) {
            try {
                mIntentInProgress = true;
                startIntentSenderForResult(mConnectionResult.getResolution()
                        .getIntentSender(), RC_SIGN_IN, null, 0, 0, 0);
            } catch (SendIntentException e) {
                mIntentInProgress = false;
                mGoogleApiClient.connect();
            }
        } else {
            showSimpleSnack(R.string.msg_ocorreu_erro);
        }
    }

    public void onConnectionFailed(ConnectionResult result) {
        if (!mIntentInProgress) {
            mConnectionResult = result;

            if (mSignInClicked) {
                resolveSignInError();
            }
        }
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        mSignInClicked = false;
        btnConectarGoogle.setText(R.string.txt_desconectar_com_google);
        btnConectarGoogle.setBackgroundResource(R.drawable.bg_button_g_connected);
        if (Plus.PeopleApi.getCurrentPerson(mGoogleApiClient) != null) {

            if (retrieveTokenTask != null) {
                retrieveTokenTask.cancel(true);
            }
            retrieveTokenTask = new RetrieveTokenTask();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                retrieveTokenTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else {
                retrieveTokenTask.execute();
            }

        } else {
            showError(null, true);
            disconnectGooglePlus();
        }
        dismissWaitDialog(true);
    }


    private class RetrieveTokenTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showWaitDialog();
        }

        @Override
        protected String doInBackground(String... params) {
            String token = "";
            try {
                String scopes = "oauth2:profile email";
                token = GoogleAuthUtil.getToken(getApplicationContext(), Plus.AccountApi.getAccountName(mGoogleApiClient), scopes);
            } catch (IOException e) {
            } catch (UserRecoverableAuthException e) {
            } catch (GoogleAuthException e) {
            }
            return token;
        }


        @Override
        protected void onPostExecute(String accessToken) {
            super.onPostExecute(accessToken);
            if (accessToken != null) {
                callGooglePlusConnect(accessToken);
            }

        }
    }

    private void callGooglePlusConnect(String accessToken) {
        Person currentPerson = Plus.PeopleApi.getCurrentPerson(mGoogleApiClient);
        if (currentPerson != null) {
            String personName = currentPerson.getDisplayName();
            String email = Plus.AccountApi.getAccountName(mGoogleApiClient);

            dismissWaitDialog(true);

            callLoginCadastroService(Constants.GOOGLE_PLUS, personName, email, accessToken, "");
        }

    }

    public void onConnectionSuspended(int cause) {
        mGoogleApiClient.connect();
    }

    private View.OnClickListener googlePlusClick = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            if (mGoogleApiClient.isConnected()) {
                showWaitDialog();
                disconnect();
            } else {
                if (!mGoogleApiClient.isConnecting()) {
                    mSignInClicked = true;
                    resolveSignInError();
                }
            }
        }
    };

    private void disconnectGooglePlus() {

        if (mGoogleApiClient.isConnected()) {
            showWaitDialog();
            Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
            Plus.AccountApi.revokeAccessAndDisconnect(mGoogleApiClient)
                    .setResultCallback(new ResultCallback<Status>() {

                        @Override
                        public void onResult(Status result) {
                            if (result.isSuccess()) {
                                mGoogleApiClient.disconnect();
                                btnConectarGoogle.setText(R.string.txt_conectar_com_google);
                                btnConectarGoogle.setBackgroundResource(R.drawable.bg_button);
                                Preferences.setToken(SettingsActivity.this, "");
                            } else {
                                Toast.makeText(SettingsActivity.this, "Erro ao desconectar!", Toast.LENGTH_LONG).show();
                            }
                            dismissWaitDialog(true);
                        }

                    });
        }

    }


    //FACEBOOK

    private List<String> fbPermissions = new ArrayList<String>();
    private UiLifecycleHelper uiHelper;

    private void initFacebook(Bundle savedInstanceState) {
        btnConectarFacebook = (LoginButton) findViewById(R.id.btn_conectar_facebook);

        uiHelper = new UiLifecycleHelper(this, callback);
        uiHelper.onCreate(savedInstanceState);
        fbPermissions.add("email");

        btnConectarFacebook.setReadPermissions(fbPermissions);
        if (isFbLoggedIn()) {
            btnConectarFacebook.setBackgroundResource(R.drawable.bg_button_fb_connected);
        } else {
            btnConectarFacebook.setBackgroundResource(R.drawable.bg_button);
        }
        btnConectarFacebook.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
        btnConectarFacebook.setTextAppearance(SettingsActivity.this, android.R.style.TextAppearance_DeviceDefault_Medium);
        FontUtilCache.setCustomFont(SettingsActivity.this, btnConectarFacebook, "fonts/ClioSemiBold-SemiBold.otf");
        btnConectarFacebook.setTextColor(Color.WHITE);
        btnConectarFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                disconnectGooglePlus();
            }
        });
    }

    public boolean isFbLoggedIn() {
        Session session = Session.getActiveSession();
        return (session != null && session.isOpened());
    }

    @SuppressWarnings("deprecation")
    private void onSessionStateChange(final Session session, SessionState state, Exception exception) {
        if (state.isOpened()) {

            Request request = Request.newMeRequest(session,
                    new Request.GraphUserCallback() {
                        // callback after Graph API response with user object

                        @Override
                        public void onCompleted(GraphUser user,
                                                Response response) {
                            if (user != null) {
                                String fbId = user.getId();
                                String fbName = user.getName();
                                if (user.asMap() != null && user.asMap().get("email") != null) {
                                    String email = user.asMap().get("email").toString();
                                    callLoginCadastroService(Constants.FACEBOOK, fbName, email, session.getAccessToken(), fbId);
                                    btnConectarFacebook.setText(R.string.txt_desconectar_com_facebook);
                                    btnConectarFacebook.setBackgroundResource(R.drawable.bg_button_fb_connected);
                                } else {
                                    showError(null, true);
                                    callFacebookLogout(SettingsActivity.this);
                                }
                            } else {
                                showError(null, true);
                                disconnect();
                            }
                        }
                    });
            Request.executeBatchAsync(request);
        } else if (state.isClosed()) {
            Log.i(TAG, "Logged out...");
            callFacebookLogout(this);
        }
    }

    /**
     * Logout From Facebook
     */
    public void callFacebookLogout(Context context) {
        Session session = Session.getActiveSession();
        if (session != null) {

            if (!session.isClosed()) {
                session.closeAndClearTokenInformation();
            }
        } else {
            session = new Session(context);
            Session.setActiveSession(session);

            session.closeAndClearTokenInformation();
        }

        Preferences.setToken(SettingsActivity.this, "");
        btnConectarFacebook.setBackgroundResource(R.drawable.bg_button);
    }

    private Session.StatusCallback callback = new Session.StatusCallback() {
        @Override
        public void call(Session session, SessionState state, Exception exception) {
            onSessionStateChange(session, state, exception);
        }
    };

    @Override
    protected int hideMenu() {
        return R.id.action_settings;
    }

    @Override
    protected boolean homeAsUpEnabled() {
        return false;
    }

    protected boolean customOpenClose() {
        return true;
    }
}
