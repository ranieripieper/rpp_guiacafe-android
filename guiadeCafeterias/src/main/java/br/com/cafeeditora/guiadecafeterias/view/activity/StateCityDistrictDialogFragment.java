package br.com.cafeeditora.guiadecafeterias.view.activity;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import com.doisdoissete.android.util.ddsutil.StringUtil;
import com.doisdoissete.android.util.ddsutil.view.custom.util.FontUtilCache;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import br.com.cafeeditora.guiadecafeterias.R;
import kankan.wheel.widget.OnWheelChangedListener;
import kankan.wheel.widget.OnWheelScrollListener;
import kankan.wheel.widget.WheelView;
import kankan.wheel.widget.adapters.ArrayWheelAdapter;

/**
 * Created by ranipieper on 10/1/15.
 */
public class StateCityDistrictDialogFragment extends DialogFragment {

    private WheelView wheelView;
    private SelectItemListener mSelectItemListener;
    private String[] mValues;
    private ArrayWheelAdapter<String> mAdapter;
    private String mDefaultValue = null;

    private StateCityDistrictDialogFragment() {

    }

    public static StateCityDistrictDialogFragment newInstance(String[] values, String defaultValue, SelectItemListener selectItemListener) {
        StateCityDistrictDialogFragment stateCityDistrictDialogFragment = new StateCityDistrictDialogFragment();
        stateCityDistrictDialogFragment.mValues = values;
        stateCityDistrictDialogFragment.mSelectItemListener = selectItemListener;
        stateCityDistrictDialogFragment.setStyle(DialogFragment.STYLE_NO_TITLE, R.style.DDS_Guia_Dialog_EstadoCidadeBairro);
        stateCityDistrictDialogFragment.mDefaultValue = defaultValue;
        return stateCityDistrictDialogFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout to use as dialog or embedded fragment
        View v = inflater.inflate(R.layout.state_city_district_dialog, container, false);

        wheelView = (WheelView) v.findViewById(R.id.wheel_view);
        wheelView.setVisibleItems(5);

        List<String> values = new ArrayList(Arrays.asList(mValues));
        Collections.sort(values, new Comparator<String>() {
            @Override
            public int compare(String lhs, String rhs) {
                return  StringUtil.getNormalizeStr(lhs).toLowerCase().compareTo(StringUtil.getNormalizeStr(rhs).toLowerCase());
            }
        });
        mValues = new String[values.size()];
        mValues = values.toArray(mValues);
        mAdapter = new ArrayWheelAdapter(getActivity(), mValues);
        mAdapter.setTextSize(22);
        mAdapter.setTypeFace(FontUtilCache.get("fonts/Clio-Regular.otf", getActivity()));
        wheelView.setViewAdapter(mAdapter);
        mAdapter.setTextSelectedColor(Color.BLACK);
        mAdapter.setTextColor(getResources().getColor(R.color.list_text_unselected));

        wheelView.addChangingListener(new OnWheelChangedListener() {
            @Override
            public void onChanged(WheelView wheel, int oldValue, int newValue) {
                mAdapter.setCurrentItem(newValue);
                wheelView.invalidateWheel(false);
            }
        });

        wheelView.addScrollingListener(new OnWheelScrollListener() {
            @Override
            public void onScrollingStarted(WheelView wheel) {
            }

            @Override
            public void onScrollingFinished(WheelView wheel) {
                mAdapter.setCurrentItem(wheelView.getCurrentItem());
                wheelView.invalidateWheel(false);
            }
        });

        v.findViewById(R.id.bt_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemSelected(mAdapter.getItemText(wheelView.getCurrentItem()).toString());
            }
        });

        v.findViewById(R.id.bt_limpar).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemSelected(null);
            }
        });

        v.findViewById(R.id.view_top).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        WindowManager.LayoutParams p = getDialog().getWindow().getAttributes();
        p.horizontalMargin = 0;
        p.gravity = Gravity.FILL_HORIZONTAL;

        if (TextUtils.isEmpty(mDefaultValue)) {
            wheelView.setCurrentItem(0);
        } else {
            String defaultNormalize = StringUtil.getNormalizeStr(mDefaultValue);
            for (int i = 0; i < mValues.length; i++) {
                if (defaultNormalize.equalsIgnoreCase(StringUtil.getNormalizeStr(mValues[i]))) {
                    wheelView.setCurrentItem(i);
                    break;
                }
            }
        }

        return v;

    }

    private void itemSelected(String item) {
        if (mSelectItemListener != null) {
            mSelectItemListener.onSelect(item);
        }
        dismiss();
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return dialog;
    }

    public interface SelectItemListener {
        void onSelect(String item);
    }

}