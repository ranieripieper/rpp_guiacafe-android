package br.com.cafeeditora.guiadecafeterias.view.activity.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import br.com.cafeeditora.guiadecafeterias.GuiaCafeteriasApplication;
import br.com.cafeeditora.guiadecafeterias.R;
import br.com.cafeeditora.guiadecafeterias.model.Cafeteria;
import br.com.cafeeditora.guiadecafeterias.util.ColorStateUtil;
import br.com.cafeeditora.guiadecafeterias.util.RateUtil;
import br.com.cafeeditora.guiadecafeterias.view.activity.adapter.holder.ViewHolderCafeterias;

import com.doisdoissete.android.util.ddsutil.gps.GpsUtil;
import com.doisdoissete.android.util.ddsutil.view.custom.listview.PagingBaseAdapter;

public class CafeteriaMainAdapter extends PagingBaseAdapter<Cafeteria> {

	private LayoutInflater inflater;
	private Context mContext;
	private TextView txtHeaderState;
	private boolean showIndividualState = false;
	
	public CafeteriaMainAdapter(Context context, List<Cafeteria> cafeterias, TextView txtHeaderState) {
		this.mContext = context;
		this.inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		if (cafeterias == null) {
			this.items = new ArrayList<Cafeteria>();
		} else {
			this.items = cafeterias;
		}
		this.txtHeaderState = txtHeaderState;
		txtHeaderState.setText("");
	}
	
	public CafeteriaMainAdapter(Context context, List<Cafeteria> cafeterias, boolean showIndividualState) {
		this.mContext = context;
		this.inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		if (cafeterias == null) {
			this.items = new ArrayList<Cafeteria>();
		} else {
			this.items = cafeterias;
		}
		this.showIndividualState = showIndividualState;
	}

	@Override
	public int getCount() {
		if (items == null) {
			return 0;
		}
		return items.size();
	}
	
	@Override
	public int getViewTypeCount() {
		return 1;
	}

	@Override
	public int getItemViewType(int position) {		
		return 0;
	}
	
	@Override
	public Cafeteria getItem(int position) {
		return items.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		return getViewRow(position, convertView, parent);
	}
	
	private View getViewRow(int position, View convertView, ViewGroup parent) {
		View vi = convertView;
		
		ViewHolderCafeterias holder = null;

		if (vi == null) {
			vi = inflater.inflate(R.layout.main_cafeteria_row, parent, false);
			holder = new ViewHolderCafeterias();
			holder.txtNome = (TextView)vi.findViewById(R.id.txt_nome_cafeteria);
			holder.imgProfile = (ImageView)vi.findViewById(R.id.img_logo);
			holder.txtDistancia = (TextView)vi.findViewById(R.id.txt_distancia);
			holder.txtDesde = (TextView)vi.findViewById(R.id.txt_desde);
			holder.txtNrAvaliacoes = (TextView)vi.findViewById(R.id.txt_nr_avaliacoes);
			holder.txtServe = (TextView)vi.findViewById(R.id.txt_serve);
			holder.txtServeTitle = (TextView)vi.findViewById(R.id.txt_serve_title);
			holder.imgPin = (ImageView)vi.findViewById(R.id.img_pin);
			holder.imgRate1 = (ImageView)vi.findViewById(R.id.img_rate_1);
			holder.imgRate2 = (ImageView)vi.findViewById(R.id.img_rate_2);
			holder.imgRate3 = (ImageView)vi.findViewById(R.id.img_rate_3);
			holder.imgRate4 = (ImageView)vi.findViewById(R.id.img_rate_4);
			holder.imgRate5 = (ImageView)vi.findViewById(R.id.img_rate_5);
			holder.txtStateImg = (TextView)vi.findViewById(R.id.txt_state_img);
			vi.setTag(holder);
		} else {
			holder = (ViewHolderCafeterias) vi.getTag();
		}

		Cafeteria cafeteria = getItem(position);
		
		holder.txtNome.setText(cafeteria.getName());
		boolean visbileDist = false;
		if (cafeteria.getAddress() != null && cafeteria.getAddress().getGeolocation() != null
				 && cafeteria.getAddress().getGeolocation().getLatitude() != null
				 && cafeteria.getAddress().getGeolocation().getLongitude() != null) {
			String dist = GpsUtil.distance(mContext, cafeteria.getAddress().getGeolocation().getLatitude(), cafeteria.getAddress().getGeolocation().getLongitude());
			if (!TextUtils.isEmpty(dist)) {
				visbileDist = true;
				holder.txtDistancia.setText(dist);
			}
		}
		
		if (visbileDist) {
			holder.txtDistancia.setVisibility(View.VISIBLE);
			holder.imgPin.setVisibility(View.VISIBLE);
			
		} else {
			holder.txtDistancia.setVisibility(View.INVISIBLE);
			holder.imgPin.setVisibility(View.INVISIBLE);
		}
		
		if (cafeteria.getEstablishment() != null && !TextUtils.isEmpty(cafeteria.getEstablishment().getOpening())) {
			holder.txtDesde.setText(cafeteria.getEstablishment().getOpening());
		} else {
			holder.txtDesde.setVisibility(View.GONE);
		}
		
		if (cafeteria.getInformations() != null) {
			holder.txtServe.setText(cafeteria.getInformations().getServe());
		} else {
			holder.txtServe.setText("");
		}
		
		int color = ColorStateUtil.getColorByState(mContext.getString(cafeteria.getAddress().getStateToCompare()), mContext);
		
		if (cafeteria.getAddress() != null && !TextUtils.isEmpty(cafeteria.getAddress().getState())) {
			String stateToCompare = mContext.getString(cafeteria.getAddress().getStateToCompare());
			holder.imgPin.setImageResource(ColorStateUtil.getPinByState(stateToCompare));
			holder.txtDistancia.setTextColor(color);
			holder.txtServeTitle.setTextColor(color);
			
			if (txtHeaderState != null) {
				if (TextUtils.isEmpty(txtHeaderState.getText())) {
					txtHeaderState.setText(stateToCompare);
					txtHeaderState.setBackgroundColor(color);
					txtHeaderState.setVisibility(View.VISIBLE);
				} else if (txtHeaderState.getText().equals(stateToCompare)) {
					txtHeaderState.setVisibility(View.VISIBLE);
				} else if (!txtHeaderState.getText().equals(stateToCompare)) {
					txtHeaderState.setVisibility(View.GONE);
				}
			}
			
			if (showIndividualState) {
				holder.txtStateImg.setText(cafeteria.getAddress().getStateToCompare());
				holder.txtStateImg.setBackgroundColor(ColorStateUtil.getBgColorByState(stateToCompare, mContext));
				holder.txtStateImg.setVisibility(View.VISIBLE);
			} else {
				holder.txtStateImg.setVisibility(View.GONE);
			}
			
		}

		if (cafeteria.getRatings() != null) {
			setImageRate(holder, cafeteria.getRatings().getAverage());
			if (cafeteria.getRatings().getTotal() != 1) {
				holder.txtNrAvaliacoes.setText(mContext.getString(R.string.txt_nr_avaliacoes, cafeteria.getRatings().getTotal()));	
			} else {
				holder.txtNrAvaliacoes.setText(mContext.getString(R.string.txt_nr_avaliacao, cafeteria.getRatings().getTotal()));
			}
			
		}
		
		GuiaCafeteriasApplication.imageLoader.displayImage(cafeteria.getLogo(), holder.imgProfile);
		
		return vi;
	}
	
	private void setImageRate(ViewHolderCafeterias holder, float rate) {
		
		RateUtil.setRate(new ImageView[] {holder.imgRate1, holder.imgRate2, holder.imgRate3, holder.imgRate4, holder.imgRate5}, 
				rate, 
				new int[]{R.drawable.icn_rate_home, R.drawable.icn_rate_almost_filled_home, R.drawable.icn_rate_filled_home});

		
	}
}