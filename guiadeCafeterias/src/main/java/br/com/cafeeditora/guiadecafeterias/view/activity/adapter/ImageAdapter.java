package br.com.cafeeditora.guiadecafeterias.view.activity.adapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import br.com.cafeeditora.guiadecafeterias.R;
import br.com.cafeeditora.guiadecafeterias.model.StringInteger;

public class ImageAdapter extends BaseAdapter {
    private Context mContext;
    private LayoutInflater inflater;
    private int layout = R.layout.include_image_view;
    
    private List<StringInteger> items = new ArrayList<StringInteger>();
    
    public ImageAdapter(Context c, List<StringInteger> items) {
        this(c, items, R.layout.include_image_view);
    }
    
    public ImageAdapter(Context c, List<StringInteger> items, int layout) {
    	this.mContext = c;
        this.items = items;
        this.layout = layout;
        inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        Collections.sort(this.items);
    }

    public int getCount() {
        return items.size();
    }

    public StringInteger getItem(int position) {
        return items.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
    	View vi = convertView;
		
    	ViewHolderImage holder = null;

		if (vi == null) {
			vi = inflater.inflate(layout, parent, false);
			holder = new ViewHolderImage();
			holder.img = (ImageView)vi.findViewById(R.id.img);
			holder.txt = (TextView)vi.findViewById(R.id.txt_text);
			vi.setTag(holder);
		} else {
			holder = (ViewHolderImage) vi.getTag();
		}

        StringInteger value = getItem(position);
        if (holder.img != null) {
        	holder.img.setImageResource(value.getNr());
        }
        
        if (holder.txt != null) {
        	holder.txt.setText(value.getStr());
        }
       
        return vi;
    }
    
    private class ViewHolderImage {
    	public ImageView img;
    	public TextView txt;
    }

}