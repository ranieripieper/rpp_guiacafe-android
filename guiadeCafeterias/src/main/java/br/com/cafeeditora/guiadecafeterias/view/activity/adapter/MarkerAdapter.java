package br.com.cafeeditora.guiadecafeterias.view.activity.adapter;

import java.io.File;
import java.util.HashMap;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import br.com.cafeeditora.guiadecafeterias.GuiaCafeteriasApplication;
import br.com.cafeeditora.guiadecafeterias.R;
import br.com.cafeeditora.guiadecafeterias.model.Cafeteria;
import br.com.cafeeditora.guiadecafeterias.util.RateUtil;

import com.doisdoissete.android.util.ddsutil.view.custom.CircularImageView;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.model.Marker;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

public class MarkerAdapter implements InfoWindowAdapter{
	private LayoutInflater mInflater;
	private HashMap<Marker, Cafeteria> items;
	private Context mContext;
	private View layoutMarker;
	
	public MarkerAdapter(LayoutInflater i, Context context, HashMap<Marker, Cafeteria> cafeterias, View layoutMarker){
	    mInflater = i;
	    this.items = cafeterias;
	    this.mContext = context;
	    this.layoutMarker = layoutMarker;
	}

	@Override
	public View getInfoContents(Marker marker) {		
	    return layoutMarker;
	}

	@Override
	public View getInfoWindow(final Marker marker) {
		final TextView txtNome = (TextView)layoutMarker.findViewById(R.id.txt_nome_cafeteria);
		final TextView txtAddress = (TextView)layoutMarker.findViewById(R.id.txt_address);
		final ImageView imgRate1 = (ImageView)layoutMarker.findViewById(R.id.img_rate_1);
		final ImageView imgRate2 = (ImageView)layoutMarker.findViewById(R.id.img_rate_2);
		final ImageView imgRate3 = (ImageView)layoutMarker.findViewById(R.id.img_rate_3);
		final ImageView imgRate4 = (ImageView)layoutMarker.findViewById(R.id.img_rate_4);
		final ImageView imgRate5 = (ImageView)layoutMarker.findViewById(R.id.img_rate_5);
		
	    final CircularImageView circularImage = (CircularImageView)layoutMarker.findViewById(R.id.img_photo);
	    circularImage.setImageResource(R.drawable.img_place_holder);
	    
	    Cafeteria item = items.get(marker);
	    if (item != null) {
	    	if (!TextUtils.isEmpty(item.getLogo())) {
		    	File f = GuiaCafeteriasApplication.imageLoader.getDiscCache().get(item.getLogo());
			    if (f != null && f.exists()) {
			    	circularImage.setImageBitmap(BitmapFactory.decodeFile(f.getAbsolutePath()));
			    } else {
			    	if (f != null & !f.exists()) {
			    		GuiaCafeteriasApplication.imageLoader.getDiscCache().remove(item.getLogo());
			    	} else {
					    GuiaCafeteriasApplication.imageLoader.displayImage(item.getLogo(), circularImage);
					    if (marker.isInfoWindowShown()) {
						    circularImage.postDelayed(new Runnable() {
						        public void run() {
						        	if (marker.isInfoWindowShown()) {
						        		marker.showInfoWindow();
						        	}
						        }
						    }, 2000);
					    }
			    	}
			    }
	    	}
			txtNome.setText(item.getName());
			txtAddress.setText(item.getFormatedAddress());
			if (item.getRatings() != null) {
				RateUtil.setRate(new ImageView[] {imgRate1, imgRate2, imgRate3, imgRate4, imgRate5}, 
						item.getRatings().getAverage(), 
						new int[]{R.drawable.icn_rate_detail_map, R.drawable.icn_rate_almost_filled_detail_map, R.drawable.icn_rate_filled_detail_map});
			}
			
			
	    }
	    
		return layoutMarker;
	}
}

