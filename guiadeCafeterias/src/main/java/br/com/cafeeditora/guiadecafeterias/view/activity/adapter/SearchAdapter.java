package br.com.cafeeditora.guiadecafeterias.view.activity.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import br.com.cafeeditora.guiadecafeterias.R;
import br.com.cafeeditora.guiadecafeterias.view.activity.adapter.holder.ViewHolderSearch;
import br.com.cafeeditora.guiadecafeterias.view.activity.adapter.model.SearchAdapterModel;

public class SearchAdapter extends BaseAdapter {
    private Context mContext;
    private LayoutInflater inflater;
    
    private List<SearchAdapterModel> items = new ArrayList<SearchAdapterModel>();
    
    public SearchAdapter(Context c, List<SearchAdapterModel> items) {
        mContext = c;
        this.items = items;
        inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return items.size();
    }

    public SearchAdapterModel getItem(int position) {
        return items.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {
    	View vi = convertView;
		
    	ViewHolderSearch holder = null;

		if (vi == null) {
			vi = inflater.inflate(R.layout.search_row, parent, false);
			holder = new ViewHolderSearch();
			holder.imgIcon = (ImageView)vi.findViewById(R.id.img_icon);
			holder.txt = (TextView)vi.findViewById(R.id.text);
			holder.radio = (RadioButton)vi.findViewById(R.id.radio);
			vi.setTag(holder);
		} else {
			holder = (ViewHolderSearch) vi.getTag();
		}
		
		final SearchAdapterModel model = getItem(position);
		
		holder.imgIcon.setImageResource(model.getIdImg());
		holder.txt.setText(model.getIdTxt());
		holder.radio.setChecked(model.isChecked());
		final RadioButton radio = holder.radio;
		
		vi.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				model.setChecked(!model.isChecked());
				radio.setChecked(model.isChecked());
			}
		});
		
		holder.radio.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				model.setChecked(!model.isChecked());
				radio.setChecked(model.isChecked());
			}
		});
		
		return vi;
    }
}
