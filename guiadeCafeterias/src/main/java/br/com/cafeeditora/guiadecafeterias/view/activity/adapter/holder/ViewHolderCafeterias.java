package br.com.cafeeditora.guiadecafeterias.view.activity.adapter.holder;

import android.widget.ImageView;
import android.widget.TextView;

public class ViewHolderCafeterias {
	
	public TextView txtNome;
	
	public TextView txtPhone;
	public ImageView imgProfile;
	public TextView txtAddress;
	public TextView txtDistancia;
	public ImageView imgEmergencia;
	public ImageView imgPromocao;
	public ImageView imgDistancia;
	public TextView txtStateImg;
	
	public TextView txtNrAvaliacoes;
	public TextView txtDesde;
	public TextView txtServe;
	public TextView txtServeTitle;
	public ImageView imgRate1;
	public ImageView imgRate2;
	public ImageView imgRate3;
	public ImageView imgRate4;
	public ImageView imgRate5;	
	public ImageView imgPin;
}
