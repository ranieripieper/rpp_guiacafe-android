package br.com.cafeeditora.guiadecafeterias.view.activity.adapter.holder;

import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

public class ViewHolderSearch {
	
	public ImageView imgIcon;
	public TextView txt;
	public RadioButton radio;
}
