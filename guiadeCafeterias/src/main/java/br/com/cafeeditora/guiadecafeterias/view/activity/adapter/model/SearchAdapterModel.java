package br.com.cafeeditora.guiadecafeterias.view.activity.adapter.model;

import java.io.Serializable;

public class SearchAdapterModel implements Serializable {

	private static final long serialVersionUID = 7188947832616301435L;

	private String paramUrl;
	private Integer idImg;
	private Integer idTxt;
	private boolean checked;
	
	public SearchAdapterModel(String paramUrl, Integer idImg, Integer idTxt, boolean checked) {
		super();
		this.paramUrl = paramUrl;
		this.idImg = idImg;
		this.idTxt = idTxt;
		this.checked = checked;
	}
	

	/**
	 * @return the idImg
	 */
	public Integer getIdImg() {
		return idImg;
	}


	/**
	 * @param idImg the idImg to set
	 */
	public void setIdImg(Integer idImg) {
		this.idImg = idImg;
	}


	/**
	 * @return the idTxt
	 */
	public Integer getIdTxt() {
		return idTxt;
	}


	/**
	 * @param idTxt the idTxt to set
	 */
	public void setIdTxt(Integer idTxt) {
		this.idTxt = idTxt;
	}


	/**
	 * @return the checked
	 */
	public boolean isChecked() {
		return checked;
	}


	/**
	 * @param checked the checked to set
	 */
	public void setChecked(boolean checked) {
		this.checked = checked;
	}


	/**
	 * @return the paramUrl
	 */
	public String getParamUrl() {
		return paramUrl;
	}

	/**
	 * @param paramUrl the paramUrl to set
	 */
	public void setParamUrl(String paramUrl) {
		this.paramUrl = paramUrl;
	}
	
}
